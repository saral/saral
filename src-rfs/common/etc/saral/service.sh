#
# SARAL
#
# Functions and variables used in service scripts.
#

#
# Set the name of service
# (Used in verbose messages)
#
SVNAME=`basename ${0}`

#
# Set the name of script
# (Used in verbose messages)
#
SCRIPT=service_${SVNAME}

#
# Start a daemon
#
saral_daemon_start()
{
	local name=$1
	local cmd=$2
	local arg=$3

	saral_label "Starting daemon: ${name}"

	start-stop-daemon -S -q -p /var/run/${name}.pid -m -x ${cmd} -- ${arg}

	saral_status $?
}


#
# Stop a daemon
#
saral_daemon_stop()
{
	local name=$1

	saral_label "Stopping daemon: ${name}"

	start-stop-daemon -K -q -o -p /var/run/${name}.pid

	saral_status $?
}


#
# Get status of the daemon
#
saral_daemon_status()
{
	local name=$1

	saral_label "Getting daemon status: ${name} - Not yet implemented"
}
