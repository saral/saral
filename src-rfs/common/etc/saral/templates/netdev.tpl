#
# SARAL
#
# Network device configuration - %DEV%
#

#
# Name of the device (System readable)
#
DEVICE=%DEV%

#
# Name of the device (Human readable)
#
NAME=%NAME%

#
# Hardware address
#
HWADDR=%HWADDR%

#
# Method used to set IP address
#
METHOD=%METHOD%

#
# IP Address (and related)
#
IPADDR=%IPADDR%
NETMASK=%NMASK%
BROADCAST=%BCAST%
