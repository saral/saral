#
# SARAL
#
# Functions and variables used in initialization scripts.
#

#
# Location of system-wide configuration
#
SARAL_DIR_SYSCONF=/etc/sysconfig

#
# Location of system-wide configuration for dynamically detected devices
# e.g. network interfaces
#
SARAL_DIR_DYNCONF=/etc/dynconfig

#
# Location of templates files.
#
SARAL_DIR_TEMPLATE=/etc/saral/templates

#
# Define ANSI sequences used
#
if [ $USE_ANSI -eq 1 ]; then

	#
	# Define commonly used colors
	#

	# Success (green)
	CLR_SUCCESS="\\033[0;32m"

	# Failure (red)
	CLR_FAILURE="\\033[0;31m"

	# Failure (yellow)
	CLR_WARNING="\\033[0;33m"

	# Logo (light blue)
	CLR_LOGO="\\033[1;34m"

	# Level indicator (light blue)
	CLR_LEVEL="\\033[1;34m"

	# Reset to normal
	CLR_RESET="\\033[0;39m"

	#
	# Set cursor at status column
	#
	CURSOR_STAT="\\033[${COL_STATUS}G"

else

	#
	# Default to empty strings
	#
	CLR_SUCCESS=""
	CLR_FAILURE=""
	CLR_WARNING=""
	CLR_LOGO=""
	CLR_LEVEL=""
	CLR_RESET=""

	#
	# Default to 3 tab spaces
	#
	CURSOR_STAT="\t\t\t"

fi


#
# Show logo
#
saral_logo()
{
	if [ $BE_QUIET -eq 1 ] ; then
		return
	fi

	echo -e "${CLR_LOGO}"
	cat /README
	echo -e "${CLR_RESET}"
}


#
# Show message header
#
saral_header()
{
	local txt=$1

	if [ $BE_QUIET -eq 1 ] ; then
		return
	fi

	echo -e " ${CLR_LEVEL}::${CLR_RESET}"
	echo -e " ${CLR_LEVEL}:: ${txt}${CLR_RESET}"
	echo -e " ${CLR_LEVEL}::${CLR_RESET}"
}


#
# Show message
#
saral_message()
{
	local txt=$1

	if [ $BE_QUIET -eq 1 ] ; then
		return
	fi

	echo -e " ${CLR_LEVEL} :${CLR_RESET}"
	echo -e " ${CLR_LEVEL} : ${txt}${CLR_RESET}"
	echo -e " ${CLR_LEVEL} :${CLR_RESET}"
}

#
# Show warning
#
saral_warn()
{
	local txt=$1

	if [ $BE_QUIET -eq 1 ] ; then
		return
	fi

	echo -e "  ${CLR_WARNING}!${CLR_RESET}"
	echo -e "  ${CLR_WARNING}! ${txt}${CLR_RESET}"
	echo -e "  ${CLR_WARNING}!${CLR_RESET}"
}


#
# Show label (corresponds to an action)
#
saral_label()
{
	local txt=$1

	if [ $BE_QUIET -eq 1 ] ; then
		return
	fi

	echo -e -n " ${CLR_LEVEL} : - ${txt}${CLR_RESET}"
}


#
# Show status (corresponds to an action)
#
saral_status()
{
	if [ $BE_QUIET -eq 1 ] ; then
		return
	fi

	if [ $1 -eq 0 ] ; then
		echo -e "${CURSOR_STAT}[${CLR_SUCCESS}SUCCESS${CLR_RESET}]"
	else
		echo -e "${CURSOR_STAT}[${CLR_FAILURE}FAILED${CLR_RESET}]"
	fi
}

#
# Show a message from a script
#
script_msg()
{
	local name=$1
	local text=$2

	echo "${name}: ${text}"
}

#
# Show a debug message from a script
#
script_dbg()
{
	local name=$1
	local text=$2

	local flag=0

	#
	# Check if debug is enabled for the script
	#
	[ -f /tmp/debug.${name} ] && flag=1

	if [ $flag -eq 1 ]; then
		echo "${name}: dbg: ${text}"
	fi
}

#
# Show an error message from a script
#
script_err()
{
	local name=$1
	local text=$2

	echo "${name}: err: ${text}"
}
