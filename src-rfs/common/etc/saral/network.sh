#
# SARAL
#
# Functions and variables used for network configuration.
#


#
# System wide network configuration
#
SARAL_CONF_NETWORK=${SARAL_DIR_SYSCONF}/network.conf

#
# Resolver configuration
#
SARAL_CONF_RESOLV=/etc/resolv.conf

#
# Directory containing network devices
#
SARAL_DIR_NETDEV=${SARAL_DIR_DYNCONF}/network

#
# Template for network device configuration
#
SARAL_TPL_NETDEV=${SARAL_DIR_TEMPLATE}/netdev.tpl

#
# Check if "ip" command from IPROUTE2 is available
#
if [ ! -x /sbin/ip ]; then
	saral_warn "Network scripts won't work without /sbin/ip."
	exit 1
fi

#
# Create directory for network devices
#
[ ! -d ${SARAL_DIR_NETDEV} ] && mkdir -p ${SARAL_DIR_NETDEV}

#
# Source system-wide network configuration, if available
#
if [ -f ${SARAL_CONF_NETWORK} ]; then
	source ${SARAL_CONF_NETWORK}
else
	NETWORKING=no
fi


#
# Convert net mask to CIDR notation
#
saral_net_mask2cidr()
{
	local mask=$1
	local cidr=0

	for i in `seq 1 4`
	do
		local num=`echo ${mask} | cut -d'.' -f${i}`

		case ${num} in
		255)	cidr=`expr ${cidr} + 8`
			;;
		254)	cidr=`expr ${cidr} + 7`
			;;
		252)	cidr=`expr ${cidr} + 6`
			;;
		248)	cidr=`expr ${cidr} + 5`
			;;
		240)	cidr=`expr ${cidr} + 4`
			;;
		224)	cidr=`expr ${cidr} + 3`
			;;
		192)	cidr=`expr ${cidr} + 2`
			;;
		128)	cidr=`expr ${cidr} + 1`
			;;
		0)
			;;
		*)	cidr=255
			break
			;;
		esac
	done

	return ${cidr}
}

#
# Check if networking is enabled. (Returns 1, if enabled).
#
saral_net_is_enabled()
{
	if [ "${NETWORKING}" == "yes" ]; then
		return 1
	fi

	return 0
}

#
# Set hostname
#
saral_net_set_hostname()
{
	local name

	if [ -f /etc/hostname ]; then
		name=`cat /etc/hostname`
	else
		if [ -z "${HOSTNAME}" ]; then
			name=saral
		else
			name=${HOSTNAME}
		fi

		echo ${name} >> /etc/hostname
	fi

	/bin/hostname ${name}

	return ${?}
}

#
# Set IP address for specified interface
#
saral_net_set_ip()
{
	local ret

	local dev=$1
	local addr=$2
	local mask=$3
	local bcast=$4

	script_msg ${SCRIPT} "Setting IP address for ${dev} - ${addr}/${mask}"

	saral_net_mask2cidr ${mask}
	local cidr=${?}

	if [ ${cidr} -eq 255 ]; then
		script_err ${SCRIPT} "saral_net_set_ip: Invalid mask - ${mask}"
		ret=1
	else
		ip addr add ${addr}/${cidr} dev ${dev} brd ${bcast}
		ret=${?}
	fi

	return ${ret}
}

#
# Clear IP address for specified interface
#
saral_net_clear_ip()
{
	local dev=$1
	local addr=$2
	local mask=$3

	script_msg ${SCRIPT} "Clearing IP address for ${dev} - ${addr}/${mask}"

	ip addr del ${addr}/${mask} dev ${dev}

	return ${?}
}

#
# Set network domain
#
# Uses following environment variables set by the DHCP client:
# - domain: Name of the network domain
# - dns   : List of DNS servers
#
saral_net_set_domain()
{
	local ret

	local conf=/tmp/.`basename ${SARAL_CONF_RESOLV}`

	#
	# Create configuration in a temporary file.
	#
	touch ${conf}

	#
	# Set domain name
	#
	[ -n "${domain}" ] && echo "search ${domain}" >> ${conf}

	#
	# Add DNS servers
	#
	for server in ${dns} ; do
		echo "nameserver ${server}" >> ${conf}
	done

	#
	# Replace existing configuration (if any).
	#
	cp -f ${conf} ${SARAL_CONF_RESOLV}
	ret=${?}

	rm -f ${conf}

	return ${ret}
}

#
# Add name server
#
saral_net_add_nameserver()
{
	local ret

	local server=$1

	if [ -n "${server}" ]; then
		echo "nameserver ${server}" >> ${SARAL_CONF_RESOLV}
		ret=${?}
	else
		ret=1
	fi

	return ${ret}
}

#
# Add route via specified gateway
#
saral_net_add_route()
{
	local dest=$1
	local gway=$2
	local dev=$3
	local args=""

	[ -n "${dev}" ] && args="dev ${dev}"

	ip route add ${dest} via ${gway} ${args}

	return ${?}
}

#
# Delete route
#
saral_net_del_route()
{
	local dest=$1
	local dev=$2
	local args=""

	[ -n "${dev}" ] && args="dev ${dev}"

	ip route del ${dest} ${args}

	return ${?}
}

#
# Check if the network device is up. (Returns 1, if device is up)
#
saral_net_is_device_up()
{
	local ret
	local dev=$1

	if [ -d /sys/class/net/${dev} ]; then
		ip -o link show dev ${dev} 2> /dev/null | grep -q ",UP"
		if [ ${?} -eq 0 ]; then
			return 1
		else
			return 0
		fi
	else
		return 0
	fi
}

#
# Configure specified network device
#
saral_net_config_device()
{
	local dev=$1
	local method=$2
	local addr=$3
	local mask=$4
	local bcast=$5

	if [ "${dev}" == "" ]; then
		script_err ${SCRIPT} "saral_net_config_device: No device specified."
		return 1
	fi

	if [ "${method}" == "" ]; then
		script_err ${SCRIPT} "saral_net_config_device: Method missing."
		return 1
	fi

	#
	# Copy template
	#
	local file=${SARAL_DIR_NETDEV}/${dev}

	cp ${SARAL_TPL_NETDEV} ${file}

	#
	# Set the name of device
	#
	sed -i	\
		-e "s/%DEV%/${dev}/g"		\
		-e "s/%NAME%/${dev}/g"		\
		${file}

	#
	# Set the MAC address
	#
	local hwaddr=`cat /sys/class/net/${dev}/address`

	sed -i -e "s/%HWADDR%/${hwaddr}/g" ${file}

	#
	# Set the method for getting IP address
	#
	sed -i -e "s/%METHOD%/${method}/g" ${file}

	if [ "${method}" == "dhcp" ]; then
		#
		# Clear address fields
		#
		sed -i	\
			-e "s/%IPADDR%//g"	\
			-e "s/%NMASK%//g"	\
			-e "s/%BCAST%//g"	\
			${file}
	else
		#
		# Set address fields
		#
		sed -i	\
			-e "s/%IPADDR%/${addr}/g"	\
			-e "s/%NMASK%/${mask}/g"	\
			-e "s/%BCAST%/${bcast}/g"	\
			${file}
	fi

	return 0
}

#
# Configure all network devices
#
saral_net_config_devices()
{
	local ret=0

	local force=$1
	local entry

	for entry in /sys/class/net/* ; do

		local dev=`basename ${entry}`

		if [ ! -f ${SARAL_DIR_NETDEV}/${dev} ]; then
			saral_message "Configuring \"${dev}\""

			if [ "${dev}" == "lo" ]; then
				saral_net_config_device lo static \
					127.0.0.1 \
					255.0.0.0 \
					127.255.255.255
				ret=${?}
			else
				saral_net_config_device ${dev} dhcp
				ret=${?}
			fi
		else
			script_msg ${SCRIPT} "saral_net_config_devices: ${dev}: Configuration exists."
		fi

		[ ${ret} -ne 0 ] && break
	done

	return ${ret}
}

#
# Start specified network device
#
saral_net_start_device()
{
	local ret=0
	local dev=$1
	local conf

	if [ "${dev}" == "" ]; then
		script_err ${SCRIPT} "saral_net_start_device: No device specified."
		return 1
	fi

	#
	# Get device-specific configuration
	#
	conf=${SARAL_DIR_NETDEV}/${dev}

	if [ -f ${conf} ]; then
		source ${conf}
	else
		script_err ${SCRIPT} "saral_net_start_device: ${dev}: Configuration not found."
		return 1
	fi

	#
	# Enable device
	#
	ip link set up dev ${dev}

	if [ "${METHOD}" == "static" ]; then
		saral_net_set_ip ${dev} ${IPADDR} ${NETMASK} ${BROADCAST}
		ret=${?}
	else
		if [ ! -x /sbin/udhcpc ]; then
			script_err ${SCRIPT} "saral_net_start_device: Couldn't find /sbin/udhcpc."
			return 1
		fi

		#
		# Get the IP address
		#
		local script="/etc/udhcpc/default.script"
		local pidfile="/var/run/udhcpc-${dev}.pid"
		local logfile="/var/log/udhcpc-${dev}.log"

		[ -f ${logfile} ] && rm -f ${logfile}

		/sbin/udhcpc -S -i ${dev} -p ${pidfile} -s ${script} > ${logfile} 2>&1
		ret=${?}
	fi

	#
	# Clear device-specific configuration sourced for the interface
	#
	unset DEVICE
	unset NAME
	unset METHOD
	unset HWADDR
	unset IPADDR
	unset NETMASK
	unset BROADCAST

	return ${ret}
}

#
# Enable all network devices
#
saral_net_start_devices()
{
	local ret=0

	local entry
	local force=$1

	for entry in /sys/class/net/* ; do

		local dev=`basename ${entry}`

		saral_net_start_device ${dev}
		ret=${?}

		[ ${ret} -ne 0 ] && break
	done

	return ${ret}
}

#
# Stop specified network device
#
saral_net_stop_device()
{
	local dev=$1

	if [ "${dev}" == "" ]; then
		script_err ${SCRIPT} "saral_net_stop_device: No device specified."
		return 1
	fi

	ip link set ${dev} down

	return ${?}
}

#
# Stop all network devices
#
saral_net_stop_devices()
{
	local ret=0

	local entry
	local force=$1

	for entry in /sys/class/net/* ; do

		local dev=`basename ${entry}`

		saral_net_stop_device ${dev}
		ret=${?}

		[ ${ret} -ne 0 ] && break
	done

	return ${ret}
}
