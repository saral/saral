#
# SARAL
#
# This script is executed when udhcpc negotiates lease with the server.
# udhcpc passes the network params as environment variables to this script.
#
# Parameters are passed to this script as environment variables.
#
# Commonly used parameters are:
#	interface	The interface.
#	ip		The client IP address.
#	subnet		The subnet mask.
#	router		The list of routers.
#	namesvr		The list of IEN 116 name servers.
#	dns		The list of DNS servers.
#	hostname	The host name.
#	domain		The domain name of the network.
#	broadcast	The broadcast address.
#	lease		The lease time in seconds.
#	serverid	The server IP address.
#	message		Reason for a DHCPNAK.
#


#
# Set name of the script
#
SCRIPT=udhcpc

#
# Include common functions
#
source /etc/saral/config.sh
source /etc/saral/include.sh

#
# If no arguments, return immediately
#
[ $# -eq 0 ] && script_err ${SCRIPT} "No arguments" && exit 1

#
# Include common network related functions
#
source /etc/saral/network.sh

#
# Initialize the routing table
#
udhcpc_init_route()
{
	local intf=$1

	script_msg "Flushing existing routes through ${intf}"
	ip route flush dev ${intf} global
	ip route flush dev ${intf} host

	local metric=0
	script_msg "Adding routes through ${intf}"
	for i in ${router} ; do
		ip route add default via ${i} dev ${intf} metric ${metric}
		metric=`expr ${metric} + 1`
	done
}


#
# Begin processing
#
[ -z "${subnet}" ] && subnet="255.255.255.0"
[ -z "${broadcast}" ] && broadcast="+"

case "${1}" in
	"deconfig")
		saral_net_set_ip ${interface} 0.0.0.0
		;;

	"renew"|"bound")
		saral_net_set_ip ${interface} ${ip} ${subnet} ${broadcast}

		[ -n "${router}" ] && udhcpc_init_route ${interface}

		saral_net_set_domain
		;;

	"nak")
		script_msg ${SCRIPT} "Received NAK (${message})"
		;;

	*)
		script_err ${SCRIPT} "Cannot handle - ${1}"
	;;

esac

exit 0
