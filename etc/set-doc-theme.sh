#!/bin/bash
#
# saral/etc/set-doc-theme.sh
#
# Set documentation theme
#
#
# The MIT License
#
# Copyright (c) 2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# CONSTANTS
# ==============================================================================

#
# Success and failure codes
#
S_SUCCESS=1
S_FAILURE=0

#
# Default template for theme
#
S_TPL=../doc/style/theme.tpl

#
# Directory containing themes
#
S_THEMEDIR=../doc/style/themes

#
# CSS to be generated
#
S_CSS=../doc/style/saral.css


# ==============================================================================
# VARIABLES
# ==============================================================================

#
# Name of theme
#
S_THEME=""

#
# Return value
#
S_RETVAL=$S_SUCCESS

#
# Associative array containing theme elements
#
declare -A theme


# ==============================================================================
# LOCAL FUNCTIONS
# ==============================================================================

function ShowHeader
{
	echo "     ::"
	echo "     :: saral utilities"
	echo "     ::"
	echo "      : Set theme for HTML documents."
	echo "      :"
}

function ShowTheme
{
	local arg=$1

	echo "      : Selected theme - ${arg}"
	echo "      :"
}

function ShowFooter
{
	local status=$1

	echo "     ::"

	if [ S"${status}" = S"${S_SUCCESS}" ]; then
		echo "     :: Success!"
	else
		echo "     :: Failed. Try again..."
	fi

	echo "     ::"
	echo ""
}

function ShowHelp
{
	echo "     :: Usage:"
	echo "     ::"
	echo "     ::    [bash] set-doc-theme.sh <theme>"
	echo "     ::"
	echo "     :: where, theme is the name of theme to be used."
	echo "     ::"
	echo "     :: Name of theme corresponds to name of file (without"
	echo "     :: extension) in the directory SARAL/doc/style/themes."
	echo "     ::"
	echo "     :: e.g. theme 'default' corresponds to the file"
	echo "     ::      SARAL/doc/style/themes/default.theme"
	echo "     ::"

	echo ""
}

function GetTheme
{
	local retval=$S_SUCCESS
	local _arg

	#
	# Get theme
	#
	_arg=$1

	if [ "${_arg}" == "" ]; then
		retval=$S_FAILURE
	else
		ShowTheme ${_arg}

		S_THEME=${S_THEMEDIR}/${_arg}.theme

		if [ ! -f ${S_THEME} ]; then
			retval=$S_FAILURE
		fi
	fi

	return $retval
}

function ApplyTheme
{
	local k
	local v

	#
	# Copy template as stylesheet.
	#
	cp ${S_TPL} ${S_CSS}

	#
	# Apply theme
	#
	for i in ${!theme[@]}
	do
		#
		# Key (match word boundary)
		#
		k="\\b${i}\\b"

		#
		# Value
		#
		v=${theme[${i}]}

		sed -i s/$k/$v/g ${S_CSS}
	done

	#
	# Update filename
	#
	sed -i s/theme\.tpl/saral\.css/g ${S_CSS}

	#
	# Update comment
	#
	sed -i "s/Stylesheet template/Stylesheet/" ${S_CSS}

	#
	# Append name of the theme.
	#
	v=${theme['NAME']}

	sed -i "/^ \* Stylesheet/ s/$/ (Based on theme $v)./" ${S_CSS}

	return $S_SUCCESS
}

# ==============================================================================
# BEGIN
# ==============================================================================

ShowHeader

if [ $# -eq 0 ]; then
	ShowHelp

	exit ${S_SUCCESS}
fi

GetTheme ${@}
S_RETVAL=${?}

if [ ${S_RETVAL} == $S_SUCCESS ]; then
	source ${S_THEME}

	ApplyTheme
	S_RETVAL=${?}
else
	ShowHelp
fi


# ==============================================================================
# END
# ==============================================================================

#
# Delete theme array
#
unset -v theme

if [ ${S_RETVAL} -eq ${S_SUCCESS} ]; then
	ShowFooter $S_SUCCESS
else
	ShowFooter $S_FAILURE
fi

exit 0
