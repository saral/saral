/*
 * saral/doc/style/theme.tpl
 *
 * Stylesheet template for the documentation.
 *
 *
 * The MIT License
 *
 * Copyright (c) 2013 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


*
{
	margin			: 0px;
	vertical-align		: middle;
}

html
{
	font-family		: DejaVu Sans Mono, Liberation Mono, Courier New, Monospace;
	font-size		: 12pt;
}

body
{
	text-align		: center;

	background-color	: BODY-BGCOLOR;
}

a,
a:link,
a:visited
{
	color			: LINK-COLOR;
	text-decoration		: none;
}

a:hover
{
	color			: LINK-HOVER-COLOR;
}

p
{
	margin-top		: 10px;
	margin-bottom		: 5px;

	text-align		: justify;
}

ul
{
	list-style-type		: square;
}

#header
{
	padding			: 0px;

	height			: 100px;

	background-color	: HEADER-BGCOLOR;
}

#logo
{
	display			: inline-block;

	position		: fixed;
	top			: 0px;
	left			: 0px;

	width			: 400px;
	height			: 100px;

	background		: url('../images/logo.png') no-repeat;
}

#tag
{
	display			: inline-block;

	position		: fixed;
	top			: 0px;
	left			: 401px;

	width			: 400px;
	height			: 100px;

	background		: url('../images/tag.png') no-repeat;
}

#content
{
	position		: fixed;
	padding			: 5px 50px 50px 50px;

	background-color	: CONTENT-BGCOLOR;
	color			: CONTENT-COLOR;

	top			: 100px;
	bottom			: 50px;

	text-align		: left;

	overflow		: auto;
}

#content h1
{
	margin-top		: 30px;
	margin-bottom		: 10px;

	background-color	: H1-BGCOLOR;
	color			: H1-COLOR;

	border-bottom		: 4px solid H1-BORDER-COLOR;

	font-size		: 20pt;
	font-weight		: bold;
}

#content h2
{
	margin-top		: 20px;
	margin-bottom		: 5px;

	background-color	: H2-BGCOLOR;
	color			: H2-COLOR;

	border-bottom		: 4px solid H2-BORDER-COLOR;

	font-size		: 18pt;
	font-weight		: bold;
}}

#content h3
{
	margin-top		: 10px;
	margin-bottom		: 5px;

	font-size		: 16pt;
	font-weight		: bold;
}

#content p
{
	font-size		: 12pt;
}

#content ul li a
{
	display			: block;
}

#content ul.level1
{
	font-size		: 16pt;
	font-weight		: bold;
}

#content ul.level1 li
{
	margin-top		: 20px;
	margin-bottom		: 5px;
}

#content ul.level1 p
{
	margin-top		: 5px;
	margin-bottom		: 5px;

	font-size		: 12pt;
	font-style		: italic;
}

#content ul.level1 a,
#content ul.level1 a:link,
#content ul.level1 a:visited,
#content ul.level2 a,
#content ul.level2 a:link,
#content ul.level2 a:visited
{
	padding-left		: 5px;
	padding-right		: 5px;

	background-color	: TOC-BGCOLOR;
	color			: TOC-COLOR;

	border-bottom		: 1px solid TOC-BORDER-COLOR;
}

#content ul.level1 a:hover,
#content ul.level2 a:hover
{
	background-color	: TOC-HOVER-BGCOLOR;
	color			: TOC-HOVER-COLOR;
	border-bottom		: 1px solid TOC-HOVER-BORDER-COLOR;
}

#content ul.level2
{
	margin-top		: 10px;
	margin-bottom		: 5px;

	font-size		: 14pt;
	font-weight		: bold;
}

#content ul.level2 li
{
	margin-top		: 5px;
	margin-bottom		: 2px;
}

#content dt
{
	margin			: 10px 0px 10px 10px;

	font-weight		: bold;
}

#content dd
{
	margin			: 5px 10px 5px 50px;

	text-align		: justify;
}

#footer
{
	position		: fixed;

	left			: 0px;
	right			: 0px;
	bottom			: 0px;
	height			: 50px;

	background-color	: FOOTER-BGCOLOR;
}

#footer p
{
	margin			: 10px;

	color			: FOOTER-COLOR;

	text-align		: left;

	font-size		: 10pt;
	font-weight		: normal;
}

#footer strong
{
	color			: FOOTER-BOLD-COLOR;
	font-weight		: bold;
}

#footer em
{
	margin-left		: 2px;
	margin-right		: 2px;

	color			: FOOTER-EM-COLOR;

	font-size		: 8pt;
	font-weight		: bold;
}

.console
{
	width			: 800px;
	padding			: 5px;

	margin			: 10px 0px;

	background-color	: CONSOLE-BGCOLOR;
	color			: CONSOLE-COLOR;
	border			: 2px solid CONSOLE-BORDER-COLOR;


	font-size		: 14pt;
	font-weight		: normal;
}

.code
{
	width			: 800px;
	padding			: 5px;

	margin			: 10px 0px;

	background-color	: CODE-BGCOLOR;
	color			: CODE-COLOR;
	border			: 2px solid CODE-BORDER-COLOR;

	font-size		: 14pt;
	font-weight		: normal;
}

.gotoc
{
	border-top		: 1px dashed GOTOC-BORDER-COLOR;

	font-size		: 10pt !important;
	font-weight		: bold;

	text-align		: right;
}

.gotoc a,
.gotoc a:link,
.gotoc a:visited
{
	color			: GOTOC-COLOR !important;
}

.gotoc a:hover
{
	color			: GOTOC-HOVER-COLOR !important;
}
