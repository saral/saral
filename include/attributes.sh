#!/bin/bash
#
# saral/include/attributes.sh
#
# Defines functions to get and set attributes.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# Variables
# ==============================================================================

#
# Board configuration
#
S_ATTR_BOARD=""

#
# Busybox configuration
#
S_ATTR_BBCONFIG=""


# ==============================================================================
# Functions
# ==============================================================================

#
# Check if the attribute is known
#
function IsKnownAttribute
{
	local retval=$S_SUCCESS
	local _arg=$1

	local _idx=0
	local _done=0
	local _attr

	while [ ${_done} -eq 0 ]; do
		_attr=$(echo ${S_KNOWN_ATTRIBUTES[${_idx}]} | cut -f1 -d ":" | tr -d ' ')

		if [ "${_attr}" == "${_arg}" ]; then
			_done=1
		else
			let _idx=_idx+1

			if [ ${_idx} -ge ${#S_KNOWN_ATTRIBUTES[@]} ]; then
				_done=1
				retval=$S_FAILURE
			fi
		fi
	done

	return $retval
}


#
# Add an attribute and its value
#
function AddAttribute
{
	local retval=$S_SUCCESS

	local _arg=$1
	local _val=$2

	ShowDebug $S_DEBUG_L3 "Adding attribute"

	local _cmd="echo \"${_arg}=${_val}\" >> ${S_CUR_CONFIG}"

	ShowDebug $S_DEBUG_L2 "${_cmd}"

	eval ${_cmd}

	ShellStatus ${?}
	retval=${?}

	return ${retval}
}


#
# Modify value of an attribute
#
function ModAttribute
{
	local retval=$S_SUCCESS

	local _arg=$1
	local _val=$2

	ShowDebug $S_DEBUG_L3 "Modifying attribute"

	local _cmd="sed -i -e 's/^\(${_arg}\)=\(.*\)$/\1=${_val}/' ${S_CUR_CONFIG}"

	ShowDebug $S_DEBUG_L2 "${_cmd}"

	eval ${_cmd}

	ShellStatus ${?}
	retval=${?}

	return ${retval}
}


#
# Set attribute and its value
#
function SetAttribute
{
	local retval=$S_SUCCESS

	local _arg=$1
	local _val=$2

	ShowDebug $S_DEBUG_L2 "Setting attribute"

	#
	# Check if the property is known
	#
	IsKnownAttribute ${_arg}
	retval=${?}

	if [ ${retval} -ne $S_SUCCESS ]; then
		ShowError $S_LEVEL2 "Unknown attribute - ${_arg}"
	fi

	#
	# Check if attribute is already defined
	#
	if [ ${retval} -eq $S_SUCCESS ]; then
		grep -q ${_arg} ${S_CUR_CONFIG}

		ShellStatus ${?}

		if [ ${?} -eq $S_SUCCESS ]; then
			ModAttribute ${_arg} ${_val}
		else
			AddAttribute ${_arg} ${_val}
		fi

		retval=${?}
	fi

	return $retval
}

#
# Get value of an attribute
#
function GetAttribute
{
	local retval=$S_SUCCESS

	local _arg=$1

	ShowDebug $S_DEBUG_L2 "Getting attribute"

	#
	# Check if the property is known
	#
	IsKnownAttribute ${_arg}
	retval=${?}

	if [ ${retval} -ne $S_SUCCESS ]; then
		ShowError $S_LEVEL2 "Unknown attribute - ${_arg}"
	fi

	#
	# Check if attribute is already defined
	#
	if [ ${retval} -eq $S_SUCCESS ]; then
		grep -q ${_arg} ${S_CUR_CONFIG}

		ShellStatus ${?}
		retval=${?}

		if [ ${retval} -eq $S_SUCCESS ]; then
			S_RETSTR=$(grep ${_arg} ${S_CUR_CONFIG} | cut -d'=' -f2)
		fi
	fi

	return $retval
}

#
# Load attributes from configuration file
# Follow leads from base attributes to load other attributes
#
function LoadAttributes
{
	local retval=$S_SUCCESS
	local _tmp

	ShowInfo $S_LEVEL1 "Reading configuration file"

	#
	# Board name
	#
	S_RETSTR=""
	GetAttribute ${S_KNOWN_ATTRIBUTES[0]}
	S_ATTR_BOARD=${S_RETSTR}

	if [ -z "${S_ATTR_BOARD}" ]; then
		ShowInfo   $S_LEVEL1 "No boards selected :("
		ShowInfo   $S_LEVEL2 "Select a board before proceeding"
		ShowSpacer $S_LEVEL2

		retval=$S_FAILURE
	else
		#
		# Translate "." to "/"
		#
		S_ATTR_BOARD=$(echo ${S_ATTR_BOARD//\./\/})

		if [ ! -f ./boards/${S_ATTR_BOARD}.sh ]; then
			ShowError $S_LEVEL1 "Unknown board - ${S_ATTR_BOARD}"
			ShowSpacer $S_LEVEL1

			retval=$S_FAILURE
		fi
	fi

	#
	# Busybox configuration
	#
	if [ ${retval} -eq $S_SUCCESS ]; then
		S_RETSTR=""
		GetAttribute ${S_KNOWN_ATTRIBUTES[1]}
		S_ATTR_BBCONFIG=${S_RETSTR}

		if [ -z "${S_ATTR_BBCONFIG}" ]; then
			ShowInfo   $S_LEVEL1 "No busybox configuration selected :("
			ShowInfo   $S_LEVEL2 "Default configuration shall be used"
			ShowSpacer $S_LEVEL2
		fi
	fi

	#
	# Follow board configuration to load additional attributes
	#
	if [ ${retval} -eq $S_SUCCESS ]; then
		#
		# Load attributes from board configuration
		#
		source ./boards/${S_ATTR_BOARD}.sh

		#
		# Load attributes from arch information
		#
		_tmp=$(dirname ${S_ATTR_BOARD})

		[ -f ./boards/${_tmp}/info.sh ] &&
			source ./boards/${_tmp}/info.sh

		#
		# Load attributes from vendor information
		#
		_tmp=$(dirname ${_tmp})

		[ -f ./boards/${_tmp}/info.sh ] &&
			source ./boards/${_tmp}/info.sh
	fi

	return $retval
}
