#!/bin/bash
#
# saral/include/userconfig.sh
#
# Defines variables specific to a user.
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


#
# Base directory where 'saral' is installed.
#
S_BASE=$PWD

#
# Location of kernel sources
# Used as base to derive the 'include' path for header files
#
S_KERNEL=$S_BASE/../linux-3.0

#
# Name and location of compiler toolchain
#
S_TOOLNAME=codesourcery
S_TOOLPATH=/opt/codesourcery

#
# Target architecture
# Useful for cross-compilation
#
S_ARCH=arm
S_SUBARCH=arm-v7

#
# Identify the build architecture
# (Used only when cross-compiling)
#
S_X_BUILD=$($(which arch))-pc-linux-gnu

#
# Select the debug level
# (Should be used only for debugging)
#
# See include/common.sh for possible values
#
S_DEBUG=

#
# Creating filesystem requires "root" privileges.
# Select a mechanism used to gain necessary privileges.
#
# Possible values: fakeroot/sudo
#
S_FS_PRIVILEGE=fakeroot

#
# Size (in MB) of the generated RAMDISK
#
S_RDISK_SIZE=64

#
# Block size (in bytes) of the generated RAMDISK
#
S_RDISK_BSIZE=1024
