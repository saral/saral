#!/bin/bash
#
# saral/include/common.sh
#
# Defines common variables and functions used across the scripts.
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# Functions
# ==============================================================================
function ShowHeader
{
	echo "     ::"
	echo "     :: saral (v$S_VERSION)"
	echo "     ::"
}

function ShowFooter
{
	local status=$1

	echo "     ::"

	if [ S"$status" = S"$S_SUCCESS" ]; then
		echo "     :: Success!"
	else
		echo "     :: Failed. Try again..."
	fi

	echo "     ::"
	echo "      : Start : ${S_TS_ENTER}"
	echo "      : Now   : ${S_TS_EXIT}"
	echo "     ::"
	echo ""
}

function Quit
{
	echo "     ::"
	echo "     :: Fatal error :("
	echo "     ::"
	echo ""

	exit
}

function ShowStep
{
	local level=$1
	local text=$2

	local prefix=""

	case $level in
	$S_LEVEL1 )
		prefix="     ::" ;;
	$S_LEVEL2 )
		prefix="      :" ;;
	$S_LEVEL3 )
		prefix="      : ." ;;
	* )
		prefix="         " ;;
	esac

	echo "$prefix"
	echo "$prefix $text"
	echo "$prefix"
}

function ShowArgument
{
	local level=$1
	local name=$2
	local value=$3

	local prefix=""

	case $level in
	$S_LEVEL1 )
		prefix="     ::" ;;
	$S_LEVEL2 )
		prefix="      :" ;;
	$S_LEVEL3 )
		prefix="      : ." ;;
	* )
		prefix="         " ;;
	esac

	echo "$prefix Argument (${name}) = ${value}"
}

function ShowInfo
{
	local level=$1
	local text=$2

	local prefix=""

	case $level in
	$S_LEVEL1 )
		prefix=" ::" ;;
	$S_LEVEL2 )
		prefix="  :" ;;
	$S_LEVEL3 )
		prefix="  : ." ;;
	$S_LEVEL4 )
		prefix="  :    ." ;;
	* )
		prefix="     " ;;
	esac

	echo "[I] $prefix $text"
}

function ShowError
{
	local level=$1
	local text=$2

	local prefix=""

	case $level in
	$S_LEVEL1 )
		prefix=" ::" ;;
	$S_LEVEL2 )
		prefix="  :" ;;
	$S_LEVEL3 )
		prefix="  : ." ;;
	$S_LEVEL4 )
		prefix="  :    ." ;;
	* )
		prefix="     " ;;
	esac

	echo "[E] $prefix $text"
}

function ShowDebug
{
	local level=$1

	if [ $S_DEBUG -gt $S_DEBUG_L0 ] && [ $level -le $S_DEBUG ]; then
		local text=$2

		local prefix=""

		case $level in
		$S_DEBUG_L1 )
			prefix="[D]  ++" ;;
		$S_DEBUG_L2 )
			prefix="[D]   +-" ;;
		$S_DEBUG_L3 )
			prefix="[D]   + +-" ;;
		* )
			prefix="[D]       " ;;
		esac

		echo "$prefix $text"
	fi
}

function ShowSpacer
{
	local level=$1

	local str=""

	case $level in
	$S_LEVEL1 )
		str="     ::" ;;
	$S_LEVEL2 )
		str="      :" ;;
	$S_LEVEL3 )
		str="      : ." ;;
	$S_LEVEL4 )
		str="      :    ." ;;
	* )
		str="" ;;
	esac

	echo "${str}"
}

function ShowToolchain
{
	ShowSpacer $S_LEVEL2

	ShowInfo $S_LEVEL2 "Using toolchain - $S_TOOLNAME"
	ShowInfo $S_LEVEL3 "Located at    - $S_TOOLPATH"

	if [ C"${S_X_COMPILE}" = "C1" ]; then
		ShowInfo $S_LEVEL3 "Cross-compile - Yes"
	else
		ShowInfo $S_LEVEL3 "Cross-compile - No"
	fi

	ShowSpacer $S_LEVEL2
}

function ShowComponent
{
	echo "     ::"
	echo "     :: Component - ${S_COMP_NAME} (v${S_COMP_VERS})"
	echo "     :: Source    - ${S_COMP_PATH} "
	echo "     ::"
}

function ShowFsRule
{
	local s=$1

	echo "      : .. ${s}"
}

function GetConfig
{
	local comp=$1

	#
	# Look for configuration file corresponding to the specified component.
	# If the file exists, we know how to build it.
	#
	local config=./configs/comp/$comp.sh

	if [ -f $config ]; then
		ShowDebug $S_DEBUG_L2 "Using configuration - $config"
		source $config
	else
		ShowError $S_LEVEL1 "$comp - Unable to find config file :("
		ShowInfo  $S_LEVEL2 "File $config doesn't exist"
		Quit
	fi
}

function DoesFunctionExist
{
	local retval=$S_SUCCESS

	local _name=$1

	type ${_name} 2>/dev/null | grep -q "is a function"

	if [ ${?} -ne 0 ]; then
		retval=$S_FAILURE
	fi

	return $retval
}

function ShellStatus
{
	local retval=$S_SUCCESS

	local _ret=$1

	if [ ${_ret} -eq 0 ]; then
		retval=$S_SUCCESS
	else
		retval=$S_FAILURE
	fi

	return $retval
}

function IsVersionControlled
{
	local retval=$S_SUCCESS

	if [ -d .git ] && [ -n "$(which git 2> /dev/null)" ]; then

		git rev-parse 2> /dev/null

		ShellStatus ${?}

		retval=${?}
	else
		retval=$S_FAILURE
	fi

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		ShowInfo $S_LEVEL4 "Sources are being version controlled."
	else
		ShowInfo $S_LEVEL4 "Sources are not version controlled."
	fi

	return $retval
}

#
# Check if specified configuration/list exist?
#
function DoesConfigExist
{
	local retval=$S_SUCCESS
	local _arg=$1

	local _cfg=./configs/list/${_arg}.cfg

	if [ ! -f ${_cfg} ]; then
		ShowError  $S_LEVEL2 "No such configuration - ${_cfg}"

		retval=$S_FAILURE
	fi

	return $retval
}
