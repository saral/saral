#!/bin/bash
#
# saral/include/tc/arm-codesourcery.sh
#
# Defines toolchain related variables specific to CodeSourcery toolchain
# for ARM architectures.
#
#
# The MIT License
#
# Copyright (c) 2009-2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

#
# Explicitly identify the toolchain as cross-compile
#
S_X_COMPILE=1

#
# Define the cross-compile prefix
#
S_X_PREFIX=arm-none-linux-gnueabi-

#
# Identify the host and target architectures
#
S_X_HOST=arm-none-linux-gnueabi
S_X_TARGET=arm-none-linux-gnueabi

#
# Map toolchain executables to generic variables
#
S_CC=${S_TOOLPATH}/bin/${S_X_PREFIX}gcc
S_CXX=${S_TOOLPATH}/bin/${S_X_PREFIX}g++
S_LD=${S_TOOLPATH}/bin/${S_X_PREFIX}ld
S_NM=${S_TOOLPATH}/bin/${S_X_PREFIX}nm
S_AR=${S_TOOLPATH}/bin/${S_X_PREFIX}ar
S_AS=${S_TOOLPATH}/bin/${S_X_PREFIX}as
S_OBJCOPY=${S_TOOLPATH}/bin/${S_X_PREFIX}objcopy
S_OBJDUMP=${S_TOOLPATH}/bin/${S_X_PREFIX}objdump
S_RANLIB=${S_TOOLPATH}/bin/${S_X_PREFIX}ranlib
S_SIZE=${S_TOOLPATH}/bin/${S_X_PREFIX}size
S_SPRITE=${S_TOOLPATH}/bin/${S_X_PREFIX}sprite
S_STRING=${S_TOOLPATH}/bin/${S_X_PREFIX}strings
S_STRIP=${S_TOOLPATH}/bin/${S_X_PREFIX}strip

#
# Version information from the toolchain.
#
S_X_VERSION=$(${S_CC} --version | head -n1)

#
# Define sysroot
#
S_SYSROOT=${S_TOOLPATH}/${S_X_TARGET}/libc

#
# Define base compiler flags based on $S_SUBARCH
# (Default is ARMv5)
#
_SUBARCH_FLAGS=""

case $S_SUBARCH in
arm-v7 )
	_SUBARCH_FLAGS="-march=armv7-a -mcpu=cortex-a8 -mfpu=neon -mfloat-abi=softfp"
	;;
* )
	_SUBARCH_FLAGS="-march=armv5"
	;;
esac

S_CFLAGS+="${_SUBARCH_FLAGS} --sysroot=${S_SYSROOT}"
S_CPPFLAGS+="${_SUBARCH_FLAGS} --sysroot=${S_SYSROOT}"
S_CXXFLAGS+="${_SUBARCH_FLAGS} --sysroot=${S_SYSROOT}"

#
# Define base linker flags
#
S_LDFLAGS="--sysroot=${S_SYSROOT}"

#
# Install sysroot on target filesystem
#
function InstallSysRoot
{
	local retval=$S_SUCCESS

	local _dst=$1
	local _src=${S_SYSROOT}

	ShowInfo $S_LEVEL4 "Using sysroot: ${_src}"

	(cd ${_src}/etc  && tar cf - .) | (cd ${_dst}/etc  && tar xf -)
	(cd ${_src}/lib  && tar cf - .) | (cd ${_dst}/lib  && tar xf -)
	(cd ${_src}/sbin && tar cf - .) | (cd ${_dst}/sbin && tar xf -)
	(cd ${_src}/usr  && tar cf - .) | (cd ${_dst}/usr  && tar xf -)

	return $retval
}
