#!/bin/bash
#
# saral/include/tc/arm-linaro.sh
#
# Defines toolchain related variables specific to Linaro toolchain
# for ARM architectures.
#
#
# The MIT License
#
# Copyright (c) 2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

#
# Explicitly identify the toolchain as cross-compile
#
S_X_COMPILE=1

#
# Define the cross-compile prefix
#
S_X_PREFIX=arm-linux-gnueabihf-

#
# Identify the host and target architectures
#
S_X_HOST=arm-linux-gnueabihf
S_X_TARGET=arm-linux-gnueabihf

#
# Map toolchain executables to generic variables
#
S_CC=${S_TOOLPATH}/bin/${S_X_PREFIX}gcc
S_CXX=${S_TOOLPATH}/bin/${S_X_PREFIX}g++
S_LD=${S_TOOLPATH}/bin/${S_X_PREFIX}ld
S_NM=${S_TOOLPATH}/bin/${S_X_PREFIX}nm
S_AR=${S_TOOLPATH}/bin/${S_X_PREFIX}ar
S_AS=${S_TOOLPATH}/bin/${S_X_PREFIX}as
S_OBJCOPY=${S_TOOLPATH}/bin/${S_X_PREFIX}objcopy
S_OBJDUMP=${S_TOOLPATH}/bin/${S_X_PREFIX}objdump
S_RANLIB=${S_TOOLPATH}/bin/${S_X_PREFIX}ranlib
S_SIZE=${S_TOOLPATH}/bin/${S_X_PREFIX}size
S_SPRITE=${S_TOOLPATH}/bin/${S_X_PREFIX}sprite
S_STRING=${S_TOOLPATH}/bin/${S_X_PREFIX}strings
S_STRIP=${S_TOOLPATH}/bin/${S_X_PREFIX}strip

#
# Version information from the toolchain.
#
S_X_VERSION=$(${S_CC} --version | head -n1)

#
# Define sysroot
#
S_SYSROOT=${S_TOOLPATH}/${S_X_TARGET}/libc


#
# Define base compiler flags based on $S_SUBARCH
# (Default is ARMv5)
#
case $S_SUBARCH in
arm-v7 )
	S_CFLAGS="-march=armv7-a -mcpu=cortex-a8 -mfpu=vfpv3 -mfloat-abi=hard"
	S_CXXFLAGS="-march=armv7-a -mcpu=cortex-a8 -mfpu=vfpv3 -mfloat-abi=hard"
	;;
* )
	S_CFLAGS="-march=armv5"
	S_CXXFLAGS="-march=armv5"
	;;
esac

S_CFLAGS+=" -O2 -fomit-frame-pointer --sysroot=${S_SYSROOT}"
S_CXXFLAGS+=" -O2 -fomit-frame-pointer --sysroot=${S_SYSROOT}"

#
# Define base linker flags
#
S_LDFLAGS="--sysroot=${S_SYSROOT}"

#
# Install sysroot on target filesystem
#
function InstallSysRoot
{
	local retval=$S_SUCCESS

	local _dst=$1
	local _src=${S_SYSROOT}/

	ShowInfo $S_LEVEL4 "Using sysroot: ${_src}"

	mkdir -p ${_dst}/lib/${S_X_TARGET}
	mkdir -p ${_dst}/usr/lib

	cp -d ${_src}/lib/*			${_dst}/lib/.
	cp -R -d ${_src}/lib/${S_X_TARGET}	${_dst}/lib/.

	cp -R -d ${_src}/sbin			${_dst}/.

	cp ${_src}/lib/pt_chown			${_dst}/lib/.
	cp -R -d ${_src}/usr/lib/${S_X_TARGET}	${_dst}/usr/lib/.
	cp -R -d ${_src}/usr/lib/locale		${_dst}/usr/lib/.


	return $retval
}
