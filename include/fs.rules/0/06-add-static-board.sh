#!/bin/bash
#
# saral/include/fs.rules/0/06-add-static-board.sh
#
# Add board-specific static content to the filesystem.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


if [ -d ${S_SRC_ROOTFS}/boards/${S_ATTR_BOARD} ]; then
	ShowFsRule "Copying static content (Board specific)"

	cp -a ${S_SRC_ROOTFS}/boards/${S_ATTR_BOARD}/* ${S_RFS_ASSEMBLE}/.

	if [ $? -ne 0 ]; then
		ShowError $S_LEVEL3 "Encountered error while copying"
		exit -1
	fi
else
	ShowInfo $S_LEVEL3 "There is no board specific content"
fi

exit 0
