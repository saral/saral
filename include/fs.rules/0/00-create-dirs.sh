#!/bin/bash
#
# saral/include/fs.rules/0/00-create-dirs.sh
#
# Create base directory structure for the filesystem.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


ShowFsRule "Creating top level directories"

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}"
[ ! -d ${S_RFS_ASSEMBLE} ] && mkdir ${S_RFS_ASSEMBLE}

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/dev"
[ ! -d ${S_RFS_ASSEMBLE}/dev ] && mkdir ${S_RFS_ASSEMBLE}/dev

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/bin"
[ ! -d ${S_RFS_ASSEMBLE}/bin ] && mkdir ${S_RFS_ASSEMBLE}/bin

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/sbin"
[ ! -d ${S_RFS_ASSEMBLE}/sbin ] && mkdir ${S_RFS_ASSEMBLE}/sbin

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/lib"
[ ! -d ${S_RFS_ASSEMBLE}/lib ] && mkdir ${S_RFS_ASSEMBLE}/lib

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/usr"
[ ! -d ${S_RFS_ASSEMBLE}/usr ] && mkdir ${S_RFS_ASSEMBLE}/usr

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/root"
[ ! -d ${S_RFS_ASSEMBLE}/root ] && mkdir ${S_RFS_ASSEMBLE}/root

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/home"
[ ! -d ${S_RFS_ASSEMBLE}/home ] && mkdir ${S_RFS_ASSEMBLE}/home

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/tmp"
[ ! -d ${S_RFS_ASSEMBLE}/tmp ] && mkdir ${S_RFS_ASSEMBLE}/tmp

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/var"
[ ! -d ${S_RFS_ASSEMBLE}/var ] && mkdir ${S_RFS_ASSEMBLE}/var

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/opt"
[ ! -d ${S_RFS_ASSEMBLE}/opt ] && mkdir ${S_RFS_ASSEMBLE}/opt

ShowDebug $S_DEBUG_L3 "${S_RFS_ASSEMBLE}/mnt"
[ ! -d ${S_RFS_ASSEMBLE}/mnt ] && mkdir ${S_RFS_ASSEMBLE}/mnt

if [ -d ${S_RFS_ASSEMBLE} ] &&
	[ -d ${S_RFS_ASSEMBLE}/dev ] &&
	[ -d ${S_RFS_ASSEMBLE}/bin ] &&
	[ -d ${S_RFS_ASSEMBLE}/sbin ] &&
	[ -d ${S_RFS_ASSEMBLE}/lib ] &&
	[ -d ${S_RFS_ASSEMBLE}/usr ] &&
	[ -d ${S_RFS_ASSEMBLE}/root ] &&
	[ -d ${S_RFS_ASSEMBLE}/home ] &&
	[ -d ${S_RFS_ASSEMBLE}/tmp ] &&
	[ -d ${S_RFS_ASSEMBLE}/var ] &&
	[ -d ${S_RFS_ASSEMBLE}/opt ] &&
	[ -d ${S_RFS_ASSEMBLE}/mnt ]; then
	exit 0
else
	exit -1
fi
