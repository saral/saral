#!/bin/bash
#
# saral/include/fs.rules/0/20-add-version-info.sh
#
# Add version information to the filesystem.
#
#
# The MIT License
#
# Copyright (c) 2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


ShowFsRule "Adding version information"

#
# Get SARAL version.
#
_v="SARAL v${S_VERSION} [$(date --utc)]\n"

#
# Add toolchain information.
#
_v+="\n"
_v+="Built with toolchain:\n"
_v+="${S_TOOL_VERS}"
_v+="\n"

#
# Save version information
#
echo -e ${_v} > ${S_RFS_ASSEMBLE}/etc/saral/version

unset _v

chmod 444 ${S_RFS_ASSEMBLE}/etc/saral/version
