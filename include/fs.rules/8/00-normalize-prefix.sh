#!/bin/bash
#
# saral/include/filters/8/00-normalize-prefix.sh
#
# While cross compiling, build rules in some packages add the
# prefix of cross-compile toolchain to the generated binaries.
# Create symbolic links to normalize the user experience.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


ShowFsRule "Normalize binaries with cross-compile prefix"

#
# List of dirs that contain executables
# Path is relative to ${S_RFS_ASSEMBLE}
#
_bindirs='bin sbin usr/bin usr/sbin usr/local/bin usr/local/sbin'

for _dir in ${_bindirs}
do
	pushd ${S_RFS_ASSEMBLE}/${_dir} > /dev/null

	_files=$(ls -1 ${S_X_PREFIX}* 2> /dev/null)

	for _f in ${_files}
	do
		_n=$(echo ${_f} | sed -e "s/${S_X_PREFIX}//")

		if [ ! -f ${_n} ] && [ ! -L ${_n} ] ; then

			ShowDebug $S_DEBUG_L3 "${_n} --> ${_f}"

			ln -s ${_f} ./${_n}
		fi
	done

	popd > /dev/null
done

exit 0
