#!/bin/bash
#
# saral/include/fs.rules/1/10-create-aux-pkgs.sh
#
# Create auxiliary packages that can be added to basic filesystem, if
# needed. This helps in reducing overall size of generated filesystem.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# Define auxiliary packages
#
# Until a better mechanism is put in place, maintain the specification of
# each auxiliary package in the code.
#
# This support will come in once install-directory is per-module - when the
# module name would be used as subdir to _install. However, current focus
# remains to get functional filesystem complete before starting there...
# ==============================================================================

S_PKG_NAME[0]="Devkit"
S_PKG_SPEC[0]="files-dev.txt"
S_PKG_REXP[0]="\.[ao]$"
S_PKG_REXP[0]+="##"
S_PKG_REXP[0]+="\/include\S*$"
S_PKG_FILE[0]="devkit.pkg"

S_PKG_NAME[1]="Help"
S_PKG_SPEC[1]="files-help.txt"
S_PKG_REXP[1]="\/man\S*$"
S_PKG_FILE[1]="help.pkg"

S_PKG_NAME[2]="TermDB"
S_PKG_SPEC[2]="files-termdb.txt"
S_PKG_REXP[2]="\/share\/terminfo\S*$"
S_PKG_FILE[2]="termdb.pkg"

S_PKG_NAME[3]="i18n"
S_PKG_SPEC[3]="files-i18n.txt"
S_PKG_REXP[3]="\/i18n\S*$"
S_PKG_REXP[3]+="##"
S_PKG_REXP[3]+="\/locale\S*$"
S_PKG_REXP[3]+="##"
S_PKG_REXP[3]+="\/gconv\S*$"
S_PKG_REXP[3]+="##"
S_PKG_REXP[3]+="\/zoneinfo\S*$"
S_PKG_FILE[3]="i18n.pkg"


# ==============================================================================
# Local helper functions
# ==============================================================================

#
# Extract the names of files matching specified pattern from input file
# into another file.
#
function DoFilter
{
	local retval=$S_SUCCESS

	local _inp=$1
	local _out=$2
	local _pat=$3

	ShowDebug $S_DEBUG_L2 "Filter (${_pat})"

	#
	# Files that match the pattern are appended to desired list
	#
	cat ${_inp} | grep -E "${_pat}" >> ${_out}

	#
	# Save filtered list into a temporary file
	#
	cat ${_inp} | grep -Ev "${_pat}" > ${_inp}.tmp

	#
	# Update the original file
	#
	cp -f ${_inp}.tmp ${_inp}

	#
	# Delete the temporary file
	#
	rm -f ${_inp}.tmp

	return $retval
}

#
# Define the extra package
#
function CreateAuxFilter
{
	local retval=$S_SUCCESS

	local _inp=$1
	local _out=$2
	local _pat=$3

	ShowDebug $S_DEBUG_L3 "Using pattern-set: (${_pat})"

	#
	# Go over each pattern contained in "_pat"
	#
	for _p in $(echo ${_pat} | tr '##' ' ')
	{
		ShowDebug $S_DEBUG_L3 "Using pattern (${_p})"

		#
		# Files/directories that match the pattern are extracted
		#
		cat ${_inp} | grep -E "${_p}" >> ${_out}

		#
		# Save filtered list into a temporary file
		#
		cat ${_inp} | grep -Ev "${_p}" > ${_inp}.tmp

		#
		# Update the original file
		#
		cp -f ${_inp}.tmp ${_inp}

		#
		# Delete the temporary file
		#
		rm -f ${_inp}.tmp
	}

	return $retval
}

#
# Create an "auxiliary" package from the specified sources.
#
# The package is just a cpio archive.
# Why? Because what is being packaged could be symbolic links.
#
function CreateAuxPkg
{
	local retval=$S_SUCCESS

	local _pkg=$1
	local _lst=$2

	ShowDebug $S_DEBUG_L3 "Create auxiliary package \"${_pkg}\""

	pushd ${S_RFS_ASSEMBLE} > /dev/null

	cat ${S_FILESYS}/${_lst} | cpio -o > ${S_AUX_BASE}/${_pkg}.cpio 2> /dev/null

	popd > /dev/null

	return $retval
}

#
# Trim the content already included in an auxiliary package.
#
function TrimAuxiliary
{
	local retval=$S_SUCCESS

	local _lst=$1

	ShowDebug $S_DEBUG_L3 "Trim auxiliary for package \"($(basename ${_lst})\""

	pushd ${S_RFS_ASSEMBLE} > /dev/null

	for _o in $(cat ${S_FILESYS}/${_lst})
	do
		if [ -d ${_o} ] ; then
			#
			# Attempt to delete a empty directories only
			#
			local _c=$(ls -1 ${_o} | wc -l)

			if [ ${_c} -eq 0 ] ; then
				ShowDebug $S_DEBUG_L3 "rmdir ${_o}"

				rmdir "${_o}"
			fi

		elif [ -f ${_o} ] ; then

			ShowDebug $S_DEBUG_L3 "rm -f ${_o}"

			rm -f "${_o}"
		fi
	done

	popd > /dev/null

	return $retval
}


# ==============================================================================
# Begin
# ==============================================================================

ret=$S_SUCCESS

_f_all=${S_FILESYS}/files-all.txt		# List of all files

ShowFsRule "Create auxiliary packages"

pushd ${S_RFS_ASSEMBLE} > /dev/null

#
# Create a list of all files
#
# Option -depth is specifically used so that directories are empty
# when deleted (unless, of course, an error occurs)
#
find . -depth -print > ${_f_all}

popd > /dev/null

pushd ${S_FILESYS} > /dev/null
#
# Save original list - just in case - for any debug or
# comparison at a later instance.
#
cp -f ${_f_all} ${_f_all}.orig

#
# Create directory for auxiliary packages
#
mkdir -p ${S_AUX_BASE}

#
# Content from specific directories shouldn't be considered.
# Filter content of these directories before we begin.
#
# - /etc
# - /bin
# - /sbin
# - /usr/bin
# - /usr/sbin
# - /usr/lib/bin
# - /usr/local/bin
#
sed -i	-e '/\.\/etc/d'			\
	-e '/\.\/bin/d'			\
	-e '/\.\/sbin/d'		\
	-e '/\.\/usr\/bin/d'		\
	-e '/\.\/usr\/sbin/d'		\
	-e '/\.\/usr\/lib\/bin/d'	\
	-e '/\.\/usr\/local\/bin/d'	\
	${_f_all}

#
# We want some file to be always present in the target filesystem.
# Filter these files as well.
#
# - /usr/share/terminfo/a/ansi
# - /usr/share/terminfo/d/dumb
# - /usr/share/terminfo/l/linux
# - /usr/share/terminfo/r/rxvt
# - usr/share/terminfo/x/xterm
# - usr/share/terminfo/x/xterm-color
# - usr/share/terminfo/x/xterm-new
# - usr/share/terminfo/v/vt100
# - usr/share/terminfo/v/vt102
#
sed -i	-e '/terminfo\/a\/ansi/d'		\
	-e '/terminfo\/d\/dumb/d'		\
	-e '/terminfo\/l\/linux/d'		\
	-e '/terminfo\/r\/rxvt/d'		\
	-e '/terminfo\/x\/xterm/d'		\
	-e '/terminfo\/x\/xterm-color/d'	\
	-e '/terminfo\/x\/xterm-new/d'		\
	-e '/terminfo\/v\/vt100/d'		\
	-e '/terminfo\/v\/vt102/d'		\
	${_f_all}

#
# Create list of files/directories that go into each "extra" package
#
local _done=0
local _idx=0

while [ ${_done} -eq 0 ]; do
	local _name=${S_PKG_NAME[${_idx}]}
	local _flt=${S_PKG_SPEC[${_idx}]}
	local _pat=${S_PKG_REXP[${_idx}]}
	local _pkg=${S_PKG_FILE[${_idx}]}

	ShowInfo $S_LEVEL4 "${_name}"

	CreateAuxFilter ${_f_all} ${_flt} ${_pat}

	CreateAuxPkg ${_pkg} ${_flt}

	TrimAuxiliary ${_flt}

	let _idx=_idx+1

	if [ ${_idx} -ge ${#S_PKG_NAME[*]} ] ; then
		_done=1
	fi

done

popd > /dev/null

# ==============================================================================
# End
# ==============================================================================

exit 0
