#!/bin/bash
#
# saral/include/fs.rules/7/00-web-catalog.sh
#
# Add version and catalog information into the default webpage.
#
#
# The MIT License
#
# Copyright (c) 2014 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


ShowFsRule "Update default webpage"

pushd ${S_RFS_ASSEMBLE} > /dev/null

_F_INDEX=var/www/index.html

_F_CATALOG=etc/saral/catalog

_T_VERSION="%SARAL_VERSION%"
_T_DETAILS="%SARAL_DETAILS%"
_T_CATALOG="%SARAL_CATALOG%"

#
# Update version information
#
sed -i "s#${_T_VERSION}#${S_VERSION}#g" ${_F_INDEX}

#
# Update toochain information
#
sed -i "s#${_T_DETAILS}#${S_TOOL_VERS}#g" ${_F_INDEX}

#
# Update package information based on the catalog
#
_HTML_CATALOG="\n      <tr><th>Package</th><td>Version</td></tr>\n"

while read _line; do
	_p=$(echo ${_line} | cut -f1 -d" ")
	_v=$(echo ${_line} | cut -f3 -d" ")

	_HTML_CATALOG+="      <tr><th>${_p}</th><td>${_v}</td></tr>\n"

done < ${_F_CATALOG}

_HTML_CATALOG+="      "

sed -i "s#${_T_CATALOG}#${_HTML_CATALOG}#g" ${_F_INDEX}

popd > /dev/null
