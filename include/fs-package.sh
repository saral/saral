#
# saral/include/fs-package.sh
#
# Defines steps for packaging the filesystem in desired format.
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


source ./include/constants.sh
source ./include/common.sh
source ./include/userconfig.sh
source ./include/derived.sh


# ==============================================================================
# VARIABLES
# ==============================================================================

#
# Name of generated package - ramdisk
#
S_GEN_RDISK=""

#
# Name of generated package - nfs
#
S_GEN_NFS=""

#
# Name of generated package - jffs2
#
S_GEN_JFFS2=""

#
# Current Revision
#
Revision=""

#
# Filesystem format
#
Format=""


# ==============================================================================
# Local helper functions
# ==============================================================================

#
# Set name of the generated filesystem packages
#
function SetPkgFilename
{
	local _ver
	local _gid

	IsVersionControlled

	if [ ${?} -eq $S_SUCCESS ]; then

		_gid=$(git log --pretty="%h" --max-count=1)
		_ver=${S_VERSION}-${_gid}
	else
		_ver=${S_VERSION}
	fi

	S_GEN_RDISK=saral-${_ver}.rd
	S_GEN_NFS=saral-${_ver}.tgz
	S_GEN_JFFS2=saral-${_ver}.jffs2
}


# ==============================================================================
# Steps to create packages
# ==============================================================================

#
# Package filesystem for mounting as a ramdisk
#
function PkgRamdisk
{
	local retval=$S_SUCCESS

	local _rd_file="./$(echo ${S_GEN_RDISK%.*})"
	local _rd_extn=$(echo ${S_GEN_RDISK##*.})
	local _rd_loop="./mnt"

	ShowStep $S_LEVEL3 "Creating RAMDISK image (${S_GEN_RDISK}) of ${S_RDISK_SIZE}MB"

	pushd ${S_FILESYS} > /dev/null

	#
	# Create a 'clear' file to hold the RAMDISK image
	#
	[ -f ${_rd_file} ] && \rm -f ${_rd_file}

	dd if=/dev/zero of=${_rd_file} bs=1M count=${S_RDISK_SIZE}

	#
	# Format the image as ext2 (for wider compatibility)
	#
	mkfs.ext2 -F -b ${S_RDISK_BSIZE} -t ext2 -L saral ${_rd_file}

	#
	# Mount the image on local mount-point
	#
	[ ! -d ${_rd_loop} ] && mkdir ${_rd_loop}

	mount -t ext2 -o loop ${_rd_file} ${_rd_loop}

	#
	# Add content
	#
	cp -a -p ${S_RFS_ASSEMBLE}/* ${_rd_loop}/.

	#
	# Unmount the image
	#
	umount ${_rd_loop}

	#
	# Compress the image for distribution
	#
	gzip -9 -S .${_rd_extn} ${_rd_file}

	ShellStatus ${?}

	if [ ${?} -eq $S_SUCCESS ]; then
		ShowStep $S_LEVEL3 "RAMDISK (${S_GEN_RDISK}) is ready."
	else
		ShowError $S_LEVEL3 "Unable to create ${S_GEN_RDISK}"
	fi

	popd > /dev/null

	return $retval
}

#
# Package filesystem for NFS mount
#
function PkgNfs
{
	local retval=$S_SUCCESS

	ShowStep $S_LEVEL3 "Creating NFS image (${S_GEN_NFS})"

	pushd ${S_RFS_ASSEMBLE} > /dev/null

	tar cfz ../${S_GEN_NFS} .

	ShellStatus ${?}

	if [ ${?} -eq $S_SUCCESS ]; then
		ShowStep $S_LEVEL3 "NFS image (${S_GEN_NFS}) is ready."
	else
		ShowError $S_LEVEL3 "Unable to create ${S_GEN_NFS}"
	fi

	popd > /dev/null

	return $retval
}

#
# Package filesystem in JFFS2 format
#
function PkgJffs2
{
	local retval=$S_FAILURE

	ShowStep $S_LEVEL3 "Creating JFFS2 image (${S_GEN_JFFS2})"

	ShowInfo $S_LEVEL4 "Not yet implemented."

	return $retval
}


# ==============================================================================
# BEGIN
# ==============================================================================

#
# Explicitly define (if not already) S_DEBUG
# Avoids many warnings during execution
#
if [ -z $S_DEBUG ]; then
	S_DEBUG=$S_DEBUG_L0
fi

#
# Check for necessary arguments
#
if [ $# -eq 0 ]; then
	ShowError $S_LEVEL2 "$(basename ${0}) - No argument specified!"

	exit 1
fi

#
# Check if argument corresponds to a supported filesystem format.
#
Format=$1

if [ "${Format}" != "RAMDISK" ] &&
   [ "${Format}" != "NFS" ] &&
   [ "${Format}" != "JFFS2" ]; then
	ShowError $S_LEVEL2 "$(basename ${0}) - Unknown argument: ${Format}"

	exit 1
fi

ShowInfo $S_LEVEL2 "Running fs.rules/9"

ret=$S_SUCCESS

for rule in $(ls -1X ${S_BASE}/include/fs.rules/9/*.sh 2> /dev/null)
do
	ShowDebug $S_DEBUG_L2 "$(basename ${rule})"
	(
		. ${rule}
	)

	if [ $? -ne 0 ] ; then
		ret=$S_FAILURE
		break
	fi
done

if [ $ret -eq $S_SUCCESS ]; then
	SetPkgFilename

	if [ "${Format}" == "RAMDISK" ]; then
		PkgRamdisk
	elif [ "${Format}" == "NFS" ]; then
		PkgNfs
	elif [ "${Format}" == "JFFS2" ]; then
		PkgJffs2
	fi

	ret=$?
else
	ret=$S_FAILURE
fi

# ==============================================================================
# END
# ==============================================================================

if [ ${ret} -eq ${S_SUCCESS} ]; then
	exit 0
else
	exit 1
fi
