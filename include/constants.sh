#!/bin/bash
#
# saral/include/constants.sh
#
# Defines constants used across saral.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

#
# Version number (N.MM[.PP])
#
# N.8x[.yy] - alpha toward M.00
# N.9x[.yy] - beta  toward M.00
# M.00[.00] - stable
# M.xx[.yy] - fixes/enhancements applying over M.00 (xx always < 50)
#
S_VERSION="0.85"

#
# Actions specified on the command-line
#
ACT_HELP=help
ACT_FETCH=fetch
ACT_SELECT=select
ACT_CONFIG=config
ACT_COMPILE=compile
ACT_INSTALL=install
ACT_BUILD=build
ACT_CLEAN=clean
ACT_MKFS=mkfs
ACT_VERS=vers

#
# List of known attributes
#
S_KNOWN_ATTRIBUTES[0]="board	: Name of target board/EVM"
S_KNOWN_ATTRIBUTES[1]="bbconfig	: Name of busybox config file"

#
# Supported filesystems
#
FS_RAMDISK=ramdisk
FS_NFS=nfs
FS_JFFS2=jffs2

#
# Success and failure codes
#
S_SUCCESS=1
S_FAILURE=0

#
# Levels used for hierarchical prints
#
S_LEVEL1=1
S_LEVEL2=2
S_LEVEL3=3
S_LEVEL4=4

#
# Debug Level
#
S_DEBUG_L0=0		# Corresponds to "No Debug"
S_DEBUG_L1=1
S_DEBUG_L2=2
S_DEBUG_L3=3

#
# Name of current configuration file
#
S_CUR_CONFIG=.saral-config

#
# Catalog of installed packages (in target filesystem)
#
S_CATALOG=/etc/saral/catalog

#
# Name of the history file
#
S_HISTORY=.saral-history

#
# Attributes in history file
#
S_ATTR_HISTORY_LIST='S_HISTORY_LIST'
S_ATTR_HISTORY_ACTION='S_HISTORY_ACTION'
S_ATTR_HISTORY_COMP='S_HISTORY_COMP'
