#!/bin/bash
#
# saral/include/history.sh
#
# Defines helper functions for resume feature.
#
#
# The MIT License
#
# Copyright (c) 2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


#
# Check if we can resume from previous checkpoint.
#
function CanResume
{
	local _arg

	#
	# Can't resume without history!
	#
	if [ ! -f ${S_HISTORY} ]; then
		ShowError $S_LEVEL2 "Can't resume. No history available."

		return $S_FAILURE
	fi

	#
	# Can resume only if requested action is same.
	#
	_arg=$(cat ${S_HISTORY} | grep ${S_ATTR_HISTORY_ACTION} | cut -f2 -d"=")

	if [ "${_arg}" != "${S_ACT}" ]; then
		ShowError $S_LEVEL2 "Can't resume. Different actions."
		ShowError $S_LEVEL3 "Action (Current) - ${S_ACT}"
		ShowError $S_LEVEL3 "Action (History) - ${_arg}"

		return $S_FAILURE
	fi

	#
	# Can resume only if list is same.
	#
	_arg=$(cat ${S_HISTORY} | grep ${S_ATTR_HISTORY_LIST} | cut -f2 -d"=")

	if [ "${_arg}" != "${S_CFG}" ]; then
		ShowError $S_LEVEL2 "Can't resume. Different lists."
		ShowError $S_LEVEL3 "List (Current) - ${S_CFG}"
		ShowError $S_LEVEL3 "List (History) - ${_arg}"

		return $S_FAILURE
	fi

	#
	# Can resume only if "resume point" can be found in the list
	# (Existence of list is already checked earlier in the sequence).
	#
	_arg=$(cat ${S_HISTORY} | grep ${S_ATTR_HISTORY_COMP} | cut -f2 -d"=")

	grep -q ${_arg} ./configs/list/${S_CFG}.cfg

	if [ ${?} -ne 0 ]; then
		ShowError $S_LEVEL2 "Can't resume. Resume point '${_arg}' not found."

		return $S_FAILURE
	fi

	return $S_SUCCESS
}


#
# Modify an element in history
#
function ModHistory
{
	local retval=$S_SUCCESS

	local _arg=$1
	local _val=$2

	ShowDebug $S_DEBUG_L3 "Modifying history"

	local _cmd="sed -i -e 's/^\(${_arg}\)=\(.*\)$/\1=${_val}/' ${S_HISTORY}"

	ShowDebug $S_DEBUG_L2 "${_cmd}"

	eval ${_cmd}

	ShellStatus ${?}
	retval=${?}

	return ${retval}
}

#
# Show name of component being skipped
#
function ShowSkip
{
	local _cmp=$1

	ShowInfo $S_LEVEL1 "Skipping '${_cmp}' ..."
}
