#!/bin/bash
#
# saral/include/steps-fetch.sh
#
# Defines functions implementing steps for fetching components.
#
#
# The MIT License
#
# Copyright (c) 2010-2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


#
# Create directory for local package cache (if it doesn't exist)
#
function CreateCache
{
	local retval=$S_SUCCESS

	if [ ! -d ${S_PKG_CACHE} ]; then
		ShowInfo $S_LEVEL2 "Creating cache directory (${S_PKG_CACHE})"

		mkdir -p ${S_PKG_CACHE}

		ShellStatus ${?}
		retval=${?}
	fi

	return $retval
}


#
# Check if source package already exists in the local cache
#
function IsSourcePkgCached
{
	local retval=$S_SUCCESS

	local _pkg=$(basename ${S_COMP_WGET})

	ShowDebug $S_DEBUG_L3 "Local cache: ${S_PKG_CACHE}"

	if [ -f ${S_PKG_CACHE}/${_pkg} ]; then
		ShowInfo $S_LEVEL2 "Found \"${_pkg}\" in local cache"
	else
		retval=$S_FAILURE
	fi

	return $retval
}


#
# Extract contents of source package
#
function ExtractPackage
{
	local retval=$S_SUCCESS

	local _pkg=$(basename ${S_COMP_WGET})
	local _dir=$(basename ${S_COMP_PATH})

	if [ -d "${_dir}" ]; then
		ShowInfo $S_LEVEL2 "Directory already exists - ${S_COMP_PATH}"
	else
		if [[ ${_pkg} =~ \.tar\.gz$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : tar.gz"

			tar xfz ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.tgz$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : tgz"

			tar xfz ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.tar\.xz$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : tar.xz"

			tar xfJ ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.tar\.bz2$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : tar.bz2"

			tar xfj ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.tar$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : .tar"

			tar xvf ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.zip$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : .zip"

			unzip -qq ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.bz2$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : .bz2"

			bunzip2 -q ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.gz$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : .gz"

			gunzip -q ${_pkg}
			retval=${?}

		elif [[ ${_pkg} =~ \.xz$ ]]; then
			ShowDebug $S_LEVEL2 "Extension : .xz"

			xz -d ${_pkg}
			retval=${?}

		else
			ShowDebug $S_LEVEL2 "Unable to extract - ${_pkg}"
			retval=1
		fi

		ShellStatus ${retval}
		retval=${?}
	fi

	return $retval
}


#
# Fetch a component
#
function DoStepsFetch
{
	local retval=$S_SUCCESS

	ShowStep $S_LEVEL1 "${S_COMP_NAME}: Fetching source package"

	if [ "${S_COMP_WGET}" = "" ]; then
		ShowError $S_LEVEL2 "No URL for source package - ${S_COMP_NAME}"
		retval=$S_FAILURE
	fi

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		IsSourcePkgCached
		retval=${?}

		if [ ${retval} -eq ${S_FAILURE} ]; then
			ShowInfo $S_LEVEL2 "Invoking wget"

			wget  --tries=5 --waitretry=30 ${S_COMP_WGET}

			ShellStatus ${?}
			retval=${?}
		fi
	fi

	return $retval
}

#
# Extract source package
#
function DoStepsExtract
{
	local retval=$S_SUCCESS

	local _pkg=$(basename ${S_COMP_WGET})


	ShowStep $S_LEVEL1 "${S_COMP_NAME}: Extracting source package"

	#
	# Copy source package from local cache
	#
	cp -f ${S_PKG_CACHE}/${_pkg} ${S_SRC_PACKAGES}/.

	ShellStatus ${?}
	retval=${?}

	if [ ${retval} -ne ${S_SUCCESS} ]; then
		ShowError $S_LEVEL2 "Couldn't copy source package - ${_pkg}"
	fi

	#
	# Extract contents
	#
	if [ ${retval} -eq ${S_SUCCESS} ]; then
		ExtractPackage
		retval=${?}
	fi

	#
	# Delete the copied source package
	# (Doesn't impact the return value)
	#
	rm -f ${S_SRC_PACKAGES}/${_pkg}

	ShellStatus ${?}
	if [ ${?} -ne ${S_SUCCESS} ]; then
		ShowError $S_LEVEL2 "Couldn't delete - ${S_SRC_PACKAGES}/${_pkg}"
	fi

	return $retval
}

#
# Fetch a component
#
function DoFetchComp
{
	local retval=$S_SUCCESS
	local _cmp=$1

	GetConfig $_cmp

	ShowComponent

	CreateCache
	retval=${?}

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		pushd ${S_PKG_CACHE} > /dev/null

		DoStepsFetch
		retval=${?}

		popd > /dev/null

		if [ ${retval} -eq ${S_SUCCESS} ]; then
			pushd ${S_SRC_PACKAGES} > /dev/null

			DoStepsExtract
			retval=${?}

			popd > /dev/null
		fi
	fi

	return $retval
}

#
# Fetch a list of components
# Calls DoFetchComp() for each component in the list
#
function DoFetchList
{
	local retval=$S_SUCCESS
	local _list=$1
	local _skip=$2

	local _cfg=./configs/list/${_list}.cfg
	local _eof=0
	local _ret

	if [ ${_skip} -eq 0 ]; then
		ModHistory ${S_ATTR_HISTORY_LIST}   ${_list}
		ModHistory ${S_ATTR_HISTORY_ACTION} ${S_ACT}
	fi

	while [ ${retval} -eq ${S_SUCCESS} ];
	do
		#
		# Read name of component
		#
		read comp

		if [ ${?} -eq 0 ]; then
			if [ ${_skip} -eq 1 ] &&
				[ "${comp}" == "${S_HISTORY_COMP}" ]; then
				_skip=0
			else
				ShowSkip ${comp}
			fi

			if [ ${_skip} -eq 0 ]; then
				ModHistory ${S_ATTR_HISTORY_COMP} ${comp}

				DoFetchComp ${comp}
				_ret=${?}

				retval=${_ret}
			fi
		else
			_eof=1
			retval=$S_FAILURE
		fi

	done < ${_cfg}

	[ ${_eof} -eq 1 ] && retval=${_ret}

	return $retval
}
