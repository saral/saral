#!/bin/bash
#
# saral/include/toolchain.sh
#
# Defines common variables and functions related to the toolchains.
# Some variables may be derived / based on 'userconfig.sh'
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


#
# Initialize the pre-processor/ compiler/ linker specific flags.
# These flags are expected to be set in toolchain specific configuation file.
#
S_CFLAGS=""
S_CPPFLAGS=""
S_CXXFLAGS=""
S_LDFLAGS=""

#
# (Re)Define the variables identifying executables in the toolchain
#
function SetToolchainVars
{
	CC=${S_CC}
	CXX=${S_CXX}
	LD=${S_LD}
	NM=${S_NM}
	AR=${S_AR}
	AS=${S_AS}
	OBJCOPY=${S_OBJCOPY}
	OBJDUMP=${S_OBJDUMP}
	RANLIB=${S_RANLIB}
	SIZE=${S_SIZE}
	SPRITE=${S_SPRITE}
	STRING=${S_STRING}
	STRIP=${S_STRIP}

	#
	# Some components discard the absolute path for cross-compile tools.
	# Update the PATH variable.
	#
	PATH=${S_TOOLPATH}/bin:${PATH}
}

#
# Define additional flags to be passed to the compiler
# Add standard paths in install directory as well
#
function SetCompilerFlags
{
	#
	# Set flags for 'C/C++' pre-processor
	#
	_CPPFLAGS="CPPFLAGS=\""
	_CPPFLAGS+="${S_CPPFLAGS}"
	_CPPFLAGS+=" -I${S_KERN_INC}"
	_CPPFLAGS+=" -I${S_INST_USRINC}"
	_CPPFLAGS+=" -I${S_INST_USRLOCALINC}"

	if [ -n "${S_COMP_CPPFLAGS}" ]; then
		_CPPFLAGS+=" ${S_COMP_CPPFLAGS}"
	fi

	_CPPFLAGS+="\""

	#
	# Set flags for 'C' compiler
	#
	_CFLAGS="CFLAGS=\""
	_CFLAGS+="${S_CFLAGS}"
	_CFLAGS+=" -I${S_KERN_INC}"
	_CFLAGS+=" -I${S_INST_USRINC}"
	_CFLAGS+=" -I${S_INST_USRLOCALINC}"

	if [ -n "${S_COMP_CFLAGS}" ]; then
		_CFLAGS+=" ${S_COMP_CFLAGS}"
	fi

	_CFLAGS+="\""

	#
	# Set flags for 'C++' compiler
	#
	_CXXFLAGS="CXXFLAGS=\""
	_CXXFLAGS+="${S_CXXFLAGS}"
	_CXXFLAGS+=" -I${S_KERN_INC}"
	_CXXFLAGS+=" -I${S_INST_USRINC}"
	_CXXFLAGS+=" -I${S_INST_USRLOCALINC}"

	if [ -n "${S_COMP_CXXFLAGS}" ]; then
		_CXXFLAGS+=" ${S_COMP_CXXFLAGS}"
	fi

	_CXXFLAGS+="\""
}

#
# Define additional flags to be passed to the linker
# Add standard paths in install directory as well
#
function SetLinkerFlags
{
	_LDFLAGS="LDFLAGS=\""
	_LDFLAGS+="${S_LDFLAGS}"
	_LDFLAGS+=" -L${S_INST_LIB}"
	_LDFLAGS+=" -L${S_INST_USRLIB}"
	_LDFLAGS+=" -L${S_INST_USRLOCALLIB}"

	if [ -n "${S_COMP_LDFLAGS}" ]; then
		_LDFLAGS+=" ${S_COMP_LDFLAGS}"
	fi

	_LDFLAGS+="\""
}

#
# Define variables to restrict pkg-config scripts to cross-compiled packages only
#
function SetPkgConfig
{
	local _cfg_sysroot
	local _cfg_libdir
	local _cfg_path

	_cfg_sysroot="PKG_CONFIG_SYSROOT_DIR=${S_INSTALL}"

	_cfg_libdir="PKG_CONFIG_LIBDIR=${S_INST_LIB}/pkgconfig"
	_cfg_libdir+=":${S_INST_USRLIB}/pkgconfig"
	_cfg_libdir+=":${S_INST_USRLOCALLIB}/pkgconfig"

	_cfg_path="PKG_CONFIG_PATH=${S_INST_LIB}/pkgconfig"
	_cfg_path+=":${S_INST_USRLIB}/pkgconfig"
	_cfg_path+=":${S_INST_USRLOCALLIB}/pkgconfig"

	_PKG_CONFIG="${_cfg_sysroot} ${_cfg_libdir} ${_cfg_path}"
}

#
# Check if S_TOOLNAME is defined
#
if [ -z "${S_TOOLNAME}" ]; then
	ShowError $S_LEVEL1 "Var S_TOOLNAME is not defined"
	ShowInfo  $S_LEVEL2 "See: include/userconfig.sh"
	Quit
fi

#
# Check if S_TOOLPATH is defined and points to valid path
#
if [ -z "${S_TOOLPATH}" ]; then
	ShowError $S_LEVEL1 "Var S_TOOLPATH is not defined"
	ShowInfo  $S_LEVEL2 "See: include/userconfig.sh"
	Quit
fi

if [ ! -d ${S_TOOLPATH} ]; then
	ShowError $S_LEVEL1 "Var S_TOOLPATH points to inexistent directory"
	ShowInfo  $S_LEVEL2 "See: include/userconfig.sh"
	Quit
fi

if [ ! -d ${S_TOOLPATH}/bin ]; then
	ShowError $S_LEVEL1 "Var S_TOOLPATH doesn't point to a valid toolchain"
	ShowInfo  $S_LEVEL2 "See: include/userconfig.sh"
	Quit
fi

#
# Check if architecture specific definitions for the toolchain exist
#
# Name of the architecture is prefixed to the toolchain to create the name of
# the file where specific tool chain related definitions are expected.
# e.g. arm-codesourcery.sh for Codesourcery toolchain for ARM.
#
_TOOL_DEFS=./include/tc/${S_ARCH}-${S_TOOLNAME}.sh

if [ -f ${_TOOL_DEFS} ]; then
	source ${_TOOL_DEFS}
else
	ShowError $S_LEVEL1 "Toolchain (${S_TOOLNAME}) not defined for ${S_ARCH}"
	ShowInfo  $S_LEVEL2 "File ${_TOOL_DEFS} doesn't exist"
	ShowInfo  $S_LEVEL2 "See: include/toolchain.sh"
	Quit
fi

#
# Check if we are cross-compiling
#
# If so, the triplet - build, host, and target - need to be set.
#
if [ C"${S_X_COMPILE}" == "C1" ]; then
	S_X_VARS="--build=${S_X_BUILD}"			# from userconfig.sh
	S_X_VARS="${S_X_VARS} --host=${S_X_HOST}"	# from ${_TOOL_DEFS}
	S_X_VARS="${S_X_VARS} --target=${S_X_TARGET}"	# from ${_TOOL_DEFS}

else
	S_X_VARS=""
fi

#
# Check if all flags were set
#
if [ "${S_CFLAGS}" == "" ]; then
	ShowError $S_LEVEL1 "Toolchain ${S_TOOLNAME}: Variable S_CFLAGS not defined."
	ShowInfo  $S_LEVEL2 "Check file ${_TOOL_DEFS}."
	Quit
fi

if [ "${S_CPPFLAGS}" == "" ]; then
	ShowError $S_LEVEL1 "Toolchain ${S_TOOLNAME}: Variable S_CPPFLAGS not defined."
	ShowInfo  $S_LEVEL2 "Check file ${_TOOL_DEFS}."
	Quit
fi

if [ "${S_CXXFLAGS}" == "" ]; then
	ShowError $S_LEVEL1 "Toolchain ${S_TOOLNAME}: Variable S_CXXFLAGS not defined."
	ShowInfo  $S_LEVEL2 "Check file ${_TOOL_DEFS}."
	Quit
fi

if [ "${S_LDFLAGS}" == "" ]; then
	ShowError $S_LEVEL1 "Toolchain ${S_TOOLNAME}: Variable S_LDFLAGS not defined."
	ShowInfo  $S_LEVEL2 "Check file ${_TOOL_DEFS}."
	Quit
fi

#
# Explicitly define common toolnames
#
S_TOOL_VARS="HOSTCC=gcc"
S_TOOL_VARS="${S_TOOL_VARS} CROSS_COMPILE=${S_X_PREFIX}"
S_TOOL_VARS="${S_TOOL_VARS} CC=${S_CC}"
S_TOOL_VARS="${S_TOOL_VARS} CXX=${S_CXX}"
S_TOOL_VARS="${S_TOOL_VARS} LD=${S_LD}"
S_TOOL_VARS="${S_TOOL_VARS} NM=${S_NM}"
S_TOOL_VARS="${S_TOOL_VARS} AR=${S_AR}"
S_TOOL_VARS="${S_TOOL_VARS} AS=${S_AS}"
S_TOOL_VARS="${S_TOOL_VARS} OBJCOPY=${S_OBJCOPY}"
S_TOOL_VARS="${S_TOOL_VARS} OBJDUMP=${S_OBJDUMP}"
S_TOOL_VARS="${S_TOOL_VARS} RANLIB=${S_RANLIB}"
S_TOOL_VARS="${S_TOOL_VARS} SIZE=${S_SIZE}"
S_TOOL_VARS="${S_TOOL_VARS} SPRITE=${S_SPRITE}"
S_TOOL_VARS="${S_TOOL_VARS} STRING=${S_STRING}"
S_TOOL_VARS="${S_TOOL_VARS} STRIP=${S_STRIP}"

#
# Check function to install sysroot is defined
#
DoesFunctionExist InstallSysRoot
if [ ${?} -ne ${S_SUCCESS} ]; then
	ShowError $S_LEVEL1 "Function 'InstallSysRoot' is not defined"
	ShowInfo  $S_LEVEL2 "See: ${_TOOL_DEFS}"
	Quit
fi

#
# Set the toolchain version
#
if [ -n "${S_X_VERSION}" ]; then
	S_TOOL_VERS=${S_X_VERSION}
else
	S_TOOL_VERS="Sorry, toolchain version not provided."
fi
