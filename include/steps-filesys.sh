#!/bin/bash
#
# saral/include/steps-filesys.sh
#
# Defines functions implementing steps for creating filesystems.
#
#
# The MIT License
#
# Copyright (c) 2009-2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


#
# Check if $S_FS_PRIVILEGE is defined
#
if [ -z "${S_FS_PRIVILEGE}" ] && [ "$(whoami)" != "root" ] ; then
	ShowError $S_LEVEL1 "Var S_FS_PRIVILEGE is not defined"
	ShowInfo $S_LEVEL2 "See: include/userconfig.sh"
	Quit
fi

#
# Check if S_FS_PRIVILEGE (if defined) refers to a valid tool
#
if [ -n "${S_FS_PRIVILEGE}" ] &&
	[ -z "$(which ${S_FS_PRIVILEGE} 2> /dev/null)" ] ; then

	ShowError $S_LEVEL1 "Var S_FS_PRIVILEGE refers to unknown tool"
	ShowInfo  $S_LEVEL2 "See: include/userconfig.sh"
	Quit
fi

#
# Define the wrapper used for creating the filesystem
#
if [ "$(whoami)" = "root" ] ; then
	S_FS_WRAPPER="bash"
else
	if [ ${S_FS_PRIVILEGE} = "sudo" ] ; then
		S_FS_WRAPPER="sudo /bin/bash"
	elif  [ ${S_FS_PRIVILEGE} = "fakeroot" ] ; then
		S_FS_WRAPPER="fakeroot -- /bin/bash"
	fi
fi


# ==============================================================================
# Local helper functions
# ==============================================================================

function DoWrappedSession
{
	local _cmd=$1
	local _arg=$2

	ShowDebug $S_DEBUG_L3 "${S_FS_WRAPPER} ${_cmd} ${_arg}"

	eval ${S_FS_WRAPPER} ${_cmd} ${_arg}

	ShellStatus ${?}

	return  ${?}
}

# ==============================================================================
# Steps to build filesystem packages
# ==============================================================================

#
# Create a RAMDISK image
#
function DoCreateRamdisk
{
	local retval=$S_SUCCESS

	DoWrappedSession ./include/fs-package.sh RAMDISK
	retval=${?}

	if [ ${retval} -eq $S_FAILURE ]; then
		ShowError $S_LEVEL2 "Failure(s) while creating RAMDISK image"
	fi

	return $retval
}

#
# Create NFS tarball
#
function DoCreateNfs
{
	local retval=$S_SUCCESS

	DoWrappedSession ./include/fs-package.sh NFS
	retval=${?}

	if [ ${retval} -eq $S_FAILURE ]; then
		ShowError $S_LEVEL2 "Failure(s) while creating NFS image"
	fi

	return $retval
}

#
# Create JFFS2 image
#
function DoCreateJffs2
{
	local retval=$S_SUCCESS

	DoWrappedSession ./include/fs-package.sh JFFS2
	retval=${?}

	if [ ${retval} -eq $S_FAILURE ]; then
		ShowError $S_LEVEL2 "Failure(s) while creating JFFS2 image"
	fi

	return $retval
}


# ==============================================================================
# Wrapper functions
# ==============================================================================

#
# Assemble the filesystem
# See include/fs.rules/README
#
function AssembleFilesys
{
	local retval=$S_SUCCESS

	local _rules_dir=${S_BASE}/include/fs.rules
	local _seq
	local _rule

	ShowStep $S_LEVEL2 "Assembling filesystem"

	if [ -d ${S_FILESYS} ]; then
		ShowStep $S_LEVEL3 "Cleaning existing content"

		#
		# Files created while building filesystem with sudo
		# privileges require same privilege level for deletion.
		#
		# When privilege level is changed from sudo to fakeroot
		# without cleaning assembled content, we would encounter
		# failure here. But, user who is downgrading privileges
		# could go back and clean. So, live with this possibility
		# for now.
		#
		if [ ${S_FS_PRIVILEGE} == "sudo" ] ; then
			sudo rm -rf ${S_FILESYS}/*
		else
			rm -rf ${S_FILESYS}/*
		fi

		if [ ${?} -ne 0 ]; then
			ShowError $S_LEVEL3 "Couldn't clean dir - ${S_FILESYS}"

			retval=$S_FAILURE
		fi
	else
		mkdir ${S_FILESYS}

		if [ ${?} -ne 0 ]; then
			ShowError $S_LEVEL3 "Couldn't create dir - ${S_FILESYS}"

			retval=$S_FAILURE
		fi
	fi

	pushd ${S_FILESYS} > /dev/null

	for _seq in  {0..8} ; do

		if [ -d ${_rules_dir}/${_seq} ] ; then

			ShowInfo $S_LEVEL2 "Running fs.rules/${_seq}"

			for _rule in $(ls -1X ${_rules_dir}/${_seq}/*.sh 2> /dev/null)
			do
				ShowDebug $S_DEBUG_L2 "$(basename ${_rule})"
				(
					. ${_rule}
				)

				if [ $? -ne 0 ] ; then
					retval=$S_FAILURE

					break 2
				fi
			done
		fi
	done

	unset _rules_dir
	unset _rule
	unset _seq

	popd > /dev/null

	return $retval
}

#
# Build the filesystem
#
function DoBuildFilesys
{
	local retval=$S_SUCCESS
	local _arg=$1

	local _ret1=$S_SUCCESS
	local _ret2=$S_SUCCESS
	local _ret3=$S_SUCCESS

	ShowStep $S_LEVEL1 "Building filesystem"

	#
	# Assemble filesystem content at one place
	#
	AssembleFilesys

	#
	# Proceed to create the filesystem images
	#
	if [ ${?} -eq ${S_SUCCESS} ]; then
		ShowStep $S_LEVEL2 "Creating filesystem images"

		if [ "${_arg}" == "" ] ||
		   [ "${_arg}" == "${FS_RAMDISK}" ]; then
			DoCreateRamdisk
			_ret1=${?}
		fi

		if [ "${_arg}" == "" ] ||
		   [ "${_arg}" == "${FS_NFS}" ]; then
			DoCreateNfs
			_ret2=${?}
		fi

		if [ "${_arg}" == "" ] ||
		   [ "${_arg}" == "${FS_JFFS2}" ]; then
			DoCreateJffs2
			_ret3=${?}
		fi

		if [ ${_ret1} -ne ${S_SUCCESS} ] ||
		   [ ${_ret2} -ne ${S_SUCCESS} ] ||
		   [ ${_ret3} -ne ${S_SUCCESS} ] ; then
			retval=$S_FAILURE
		fi
	else
		retval=$S_FAILURE
	fi

	return $retval
}
