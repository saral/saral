#!/bin/bash
#
# saral/include/steps-build.sh
#
# Defines functions implementing steps for building components.
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# Steps for configuring the components
# ==============================================================================

function DoCopy
{
	local retval=$S_SUCCESS
	local _arg=$1

	ShowStep $S_LEVEL2 "Copying configuration"

	(
		#
		# Execute the command
		#
		ShowDebug $S_DEBUG_L3 "cp ${_arg}"
		eval cp ${_arg}
	)

	if [ $? -ne 0 ]; then
		ShowError $S_LEVEL2 "Failed to copy configuration"

		retval=$S_FAILURE
	fi

	return $retval
}

function DoSed
{
	local retval=$S_SUCCESS
	local _idx=$1
	local _arg=$2

	local _chk=.saral-sed-${_idx}

	ShowStep $S_LEVEL2 "Fixing source package for cross-compilation."
	ShowInfo $S_LEVEL2 "Step ${_idx}"

	(
		#
		# Check if the step was already executed earlier
		#
		if [ ! -f ${_chk} ]; then
			#
			# Execute the command
			#
			ShowDebug $S_DEBUG_L3 "sed ${_arg}"
			eval sed -i ${_arg}
		fi
	)

	if [ $? -eq 0 ]; then
		touch ${_chk}
	else
		ShowError $S_LEVEL2 "Couldn't fix the sources :("

		retval=$S_FAILURE
	fi

	return $retval
}

function DoAutogen
{
	local retval=$S_SUCCESS
	local _env=$1
	local _args=$2

	ShowStep $S_LEVEL2 "Running 'autogen.sh'"

	(
		ShowDebug $S_DEBUG_L2 "Component flags: [${_args}]"

		#
		# Initialize local variables
		#
		local _PRE="${_env}"
		local _ARG=""

		#
		# Override definition of toolchain variables in parent shell
		#
		SetToolchainVars
		SetCompilerFlags
		SetLinkerFlags
		SetPkgConfig

		#
		# Variables to be defined before executing the command
		# (Safe duplication)
		#
		_PRE="${_PRE} ${S_TOOL_VARS}"		# Cross-compile tools
		_PRE="${_PRE} ${_CPPFLAGS}"		# C Preprocessor Flags
		_PRE="${_PRE} ${_CFLAGS}"		# Compiler flags (C)
		_PRE="${_PRE} ${_CXXFLAGS}"		# Compiler flags (C++)
		_PRE="${_PRE} ${_LDFLAGS}"		# Linker flags
		_PRE="${_PRE} ${_PKG_CONFIG}"		# Package configuration

		#
		# If script expects absolute/fixed command-line arguments,
		# don't add generic options here.
		#
		if [ -z ${S_COMP_CONF_ABS} ]; then
			_ARG="${_ARG} ${S_X_VARS}"	# Cross compile triplet
		fi

		#
		# Add component specific flags and/or options
		#
		_ARG="${_ARG} ${_args}"

		#
		# Ensure that "autogen.sh" is executable
		#
		[ -f ./autogen.sh ] && [ ! -x ./autogen.sh ] && chmod 755 ./autogen.sh

		#
		# Execute the command
		#
		ShowDebug $S_DEBUG_L3 "autogen: [${_PRE} ./autogen.sh ${_ARG}]"
		eval ${_PRE} ./autogen.sh ${_ARG}
	)

	if [ $? -ne 0 ]; then
		ShowError $S_LEVEL2 "Failure in 'autogen.sh'"

		retval=$S_FAILURE
	fi

	return $retval
}

function DoAutoreconf
{
	local retval=$S_SUCCESS
	local _env=$1
	local _args=$2

	ShowStep $S_LEVEL2 "Running 'autoreconf'"

	(
		ShowDebug $S_DEBUG_L2 "Component flags: [${_args}]"

		#
		# Initialize local variables
		#
		local _PRE="${_env}"
		local _ARG=""

		#
		# Override definition of toolchain variables in parent shell
		#
		SetToolchainVars
		SetCompilerFlags
		SetLinkerFlags
		SetPkgConfig

		#
		# Variables to be defined before executing the command
		# (Safe duplication)
		#
		_PRE="${_PRE} ${S_TOOL_VARS}"		# Cross-compile tools
		_PRE="${_PRE} ${_CPPFLAGS}"		# C Preprocessor Flags
		_PRE="${_PRE} ${_CFLAGS}"		# Compiler flags (C)
		_PRE="${_PRE} ${_CXXFLAGS}"		# Compiler flags (C++)
		_PRE="${_PRE} ${_LDFLAGS}"		# Linker flags
		_PRE="${_PRE} ${_PKG_CONFIG}"		# Package configuration

		#
		# Add component specific flags and/or options
		#
		_ARG="${_ARG} ${_args}"

		#
		# Execute the command
		#
		ShowDebug $S_DEBUG_L3 "autoreconf: [${_PRE} autoreconf ${_ARG}]"
		eval ${_PRE} autoreconf ${_ARG}
	)

	if [ $? -ne 0 ]; then
		ShowError $S_LEVEL2 "Failure in 'autoreconf'"

		retval=$S_FAILURE
	fi

	return $retval
}

function DoConfigure
{
	local retval=$S_SUCCESS
	local _env=$1
	local _args=$2

	ShowStep $S_LEVEL2 "Running 'configure'"

	(
		ShowDebug $S_DEBUG_L2 "Component flags: [${_args}]"

		#
		# Initialize local variables
		#
		local _PRE="${_env}"
		local _ARG=""

		#
		# Override definition of toolchain variables in parent shell
		#
		SetToolchainVars
		SetCompilerFlags
		SetLinkerFlags
		SetPkgConfig

		#
		# Variables to be defined before executing the command
		# (Safe duplication)
		#
		_PRE="${_PRE} ${S_TOOL_VARS}"		# Cross-compile tools
		_PRE="${_PRE} ${_CPPFLAGS}"		# C Preprocessor Flags
		_PRE="${_PRE} ${_CFLAGS}"		# Compiler flags (C)
		_PRE="${_PRE} ${_CXXFLAGS}"		# Compiler flags (C++)
		_PRE="${_PRE} ${_LDFLAGS}"		# Linker flags
		_PRE="${_PRE} ${_PKG_CONFIG}"		# Package configuration

		#
		# If script expects absolute/fixed command-line arguments,
		# don't add generic options here.
		#
		if [ -z ${S_COMP_CONF_ABS} ]; then
			_ARG="${_ARG} ${S_X_VARS}"	# Cross compile triplet
		fi

		#
		# Add component specific flags and/or options
		#
		_ARG="${_ARG} ${_args}"

		#
		# Ensure that "configure" is executable
		#
		[ -f ./configure ] && [ ! -x ./configure ] && chmod 755 ./configure

		#
		# Execute the command
		#
		ShowDebug $S_DEBUG_L3 "configure: [${_PRE} ./configure ${_ARG}]"
		eval ${_PRE} ./configure ${_ARG}
	)

	if [ $? -ne 0 ]; then
		ShowError $S_LEVEL2 "Failure in 'configure'"

		retval=$S_FAILURE
	fi

	return $retval
}


# ==============================================================================
# Steps for 'making' the components
# ==============================================================================

function DoMake
{
	local retval=$S_SUCCESS
	local _env=$1
	local _args=$2

	ShowStep $S_LEVEL2 "Running 'make'"

	(
		ShowDebug $S_DEBUG_L2 "Component flags: [${_args}]"

		#
		# Initialize local variables
		#
		local _PRE="${_env}"
		local _ARG=""

		#
		# Override definition of toolchain variables in parent shell
		#
		SetToolchainVars
		SetCompilerFlags
		SetLinkerFlags
		SetPkgConfig

		#
		# Variables to be defined before executing the command
		# (Safe duplication)
		#
		_PRE="${_PRE} ${S_TOOL_VARS}"		# Cross-compile tools
		_PRE="${_PRE} ${_CPPFLAGS}"		# C Preprocessor Flags
		_PRE="${_PRE} ${_CFLAGS}"		# Compiler flags (C)
		_PRE="${_PRE} ${_CXXFLAGS}"		# Compiler flags (C++)
		_PRE="${_PRE} ${_LDFLAGS}"		# Linker flags
		_PRE="${_PRE} ${_PKG_CONFIG}"		# Package configuration

		#
		# Add component specific flags and/or options
		#
		_ARG="${_ARG} ${_args}"

		#
		# Execute the command
		#
		ShowDebug $S_DEBUG_L3 "make: [${_PRE} make ${_ARG}]"
		eval ${_PRE} make ${_ARG}
	)

	if [ $? -ne 0 ]; then
		ShowError $S_LEVEL2 "Failure in 'make'"

		retval=$S_FAILURE
	fi

	return $retval
}


# ==============================================================================
# Steps for installing the components
# ==============================================================================

function DoMakeInstall
{
	local retval=$S_SUCCESS
	local _args=$1
	local _env=$2
	local _act

	_act=$(echo ${_args} | cut -f1 -d" ")
	_args=$(echo ${_args} | cut -f2- -d" ")

	ShowStep $S_LEVEL2 "Running 'make ${_act}'"

	(
		ShowDebug $S_DEBUG_L2 "Component flags: [${_args}]"

		#
		# Initialize local variables
		#
		local _PRE="${_env}"
		local _ARG=""

		#
		# Override definition of toolchain variables in parent shell
		#
		SetToolchainVars
		SetCompilerFlags
		SetLinkerFlags
		SetPkgConfig

		#
		# Variables to be defined before executing the command
		# (Safe duplication)
		#
		_PRE="${_PRE} ${S_TOOL_VARS}"		# Cross-compile tools
		_PRE="${_PRE} ${_CPPFLAGS}"		# C Preprocessor Flags
		_PRE="${_PRE} ${_CFLAGS}"		# Compiler flags (C)
		_PRE="${_PRE} ${_CXXFLAGS}"		# Compiler flags (C++)
		_PRE="${_PRE} ${_LDFLAGS}"		# Linker flags
		_PRE="${_PRE} ${_PKG_CONFIG}"		# Package configuration

		#
		# Add component specific flags and/or options
		#
		_ARG="${_ARG} ${_args}"

		#
		# Execute the command
		#
		ShowDebug $S_DEBUG_L3 "install: [${_PRE} make ${_act} DESTDIR=${S_INSTALL} ${_ARG}]"
		eval ${_PRE} make ${_act} DESTDIR=${S_INSTALL} ${_ARG}
	)

	if [ $? -ne 0 ]; then
		ShowError $S_LEVEL2 "Failure in 'make ${_act}'"

		retval=$S_FAILURE
	fi

	return $retval
}

# ==============================================================================
# Wrapper functions (Level 1)
# ==============================================================================

#
# Iterate through the configuration steps listed in the component's config file
# See: S_COMP_CONF_CMD[]
#
function DoStepsConfigure
{
	local retval=$S_SUCCESS

	local _cmp=$1
	local _cmd=""
	local _arg=""
	local _env=""

	ShowStep $S_LEVEL1 "${S_COMP_NAME}: Start configuration"

	local _done=0
	local _idx=0

	while [ "${_done}" = "0" ]; do

		_cmd=${S_COMP_CONF_CMD[${_idx}]}
		_arg=${S_COMP_CONF_ARG[${_idx}]}
		_env=${S_COMP_CONF_ENV[${_idx}]}

		case "${_cmd}" in
		"autogen.sh" )
			DoAutogen "${_env}" "${_arg}"
			retval=$?
			;;

		"autoreconf" )
			DoAutoreconf "${_env}" "${_arg}"
			retval=$?
			;;

		"configure" )
			DoConfigure "${_env}" "${_arg}"
			retval=$?
			;;

		"make" )
			DoMake "${_env}" "${_arg}"
			retval=$?
			;;

		"copy" )
			DoCopy "${_arg}"
			retval=$?
			;;

		"sed" )
			DoSed "${_idx}" "${_arg}"
			retval=$?
			;;

		"" )
			ShowInfo $S_LEVEL2 "No configuration required."
			;;

		* )
			ShowError $S_LEVEL2 "Unknown command [${_cmd}] :("
			retval=$S_FAILURE
		esac

		if [ ${retval} -eq ${S_FAILURE} ]; then
			_done=1
		else
			let _idx=_idx+1

			if [ ${_idx} -ge ${#S_COMP_CONF_CMD[*]} ]; then
				_done=1
			fi
		fi

	done

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Configured"
	else
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Not configured"
	fi

	return $retval
}

#
# Iterate through the build steps listed in the component's config file
# See: S_COMP_MAKE_CMD[]
#
function DoStepsCompile
{
	local retval=$S_SUCCESS

	local _cmp=$1
	local _cmd=""
	local _arg=""
	local _env=""

	ShowStep $S_LEVEL1 "${S_COMP_NAME}: Start build"

	local _done=0
	local _idx=0

	while [ "${_done}" = "0" ]; do

		_cmd=${S_COMP_MAKE_CMD[${_idx}]}
		_arg=${S_COMP_MAKE_ARG[${_idx}]}
		_env=${S_COMP_MAKE_ENV[${_idx}]}

		case "${_cmd}" in
		"make" )
			DoMake "${_env}" "${_arg}"
			retval=$?
			;;

		* )
			ShowError $S_LEVEL2 "Unknown command [${_cmd}] :("
			retval=$S_FAILURE
		esac

		if [ ${retval} -eq ${S_FAILURE} ]; then
			_done=1
		else
			let _idx=_idx+1

			if [ ${_idx} -ge ${#S_COMP_MAKE_CMD[*]} ]; then
				_done=1
			fi
		fi

	done

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Built"
	else
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Not built"
	fi

	return $retval
}

#
# Most components can be cleaned via "make clean". Until real need is felt,
# this command would be used - almost hardcoded.
#
function DoStepsClean
{
	local retval=$S_SUCCESS

	local _cmp=$1

	local _arg="clean"
	local _env=""

	ShowStep $S_LEVEL1 "${S_COMP_NAME}: Start clean"

	DoMake "${_env}" "${_arg}"
	retval=$?

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Cleaned"
	else
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Not clean."
	fi

	return $retval
}

#
# Iterate through the install steps listed in the component's config file
# See: S_COMP_INST_CMD[]
#
function DoStepsInstall
{
	local retval=$S_SUCCESS

	local _cmp=$1
	local _cmd=""
	local _arg=""
	local _env=""

	ShowStep $S_LEVEL1 "${S_COMP_NAME}: Start installation"

	local _enter=$(date +"%Y-%m-%d %H:%M")

	local _done=0
	local _idx=0

	while [ "${_done}" = "0" ]; do

		_cmd=${S_COMP_INST_CMD[${_idx}]}
		_arg=${S_COMP_INST_ARG[${_idx}]}
		_env=${S_COMP_INST_ENV[${_idx}]}

		case "${_cmd}" in
		"make" )
			DoMakeInstall "${_arg}" "${_env}"
			retval=$?
			;;

		* )
			ShowError $S_LEVEL2 "Unknown command [${_cmd}] :("
			retval=$S_FAILURE
		esac

		if [ ${retval} -eq ${S_FAILURE} ]; then
			_done=1
		else
			let _idx=_idx+1

			if [ ${_idx} -ge ${#S_COMP_INST_CMD[*]} ]; then
				_done=1
			fi
		fi

	done

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Post-processing..."

		PostProcess ${_enter}
		retval=${?}
	fi

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Installed"

		UpdateCatalog ${S_COMP_NAME} ${S_COMP_VERS}
		retval=${?}
	else
		ShowStep $S_LEVEL1 "${S_COMP_NAME}: Not installed"
	fi

	return $retval
}

# ==============================================================================
# Wrapper functions (Level 2)
# ==============================================================================

#
# Configure a component
#
function DoConfigureComp
{
	local retval=$S_SUCCESS
	local _cmp=$1

	GetConfig $_cmp

	ShowComponent

	pushd $S_COMP_PATH > /dev/null

	DoStepsConfigure
	retval=${?}

	popd > /dev/null

	#
	# Unset all component specific flags
	#
	[ -n "${S_COMP_CONF_ABS}" ] && unset S_COMP_CONF_ABS

	[ -n "${S_COMP_CONF_ENV}" ] && unset S_COMP_CONF_ENV
	[ -n "${S_COMP_CONF_CMD}" ] && unset S_COMP_CONF_CMD
	[ -n "${S_COMP_CONF_ARG}" ] && unset S_COMP_CONF_ARG

	return $retval
}

#
# Configure a list of components
# Calls DoConfigComp() for each component in the list
#
function DoConfigureList
{
	local retval=$S_SUCCESS
	local _list=$1
	local _skip=$2

	local _cfg=./configs/list/${_list}.cfg
	local _eof=0
	local _ret

	if [ ${_skip} -eq 0 ]; then
		ModHistory ${S_ATTR_HISTORY_LIST}   ${_list}
		ModHistory ${S_ATTR_HISTORY_ACTION} ${S_ACT}
	fi

	while [ ${retval} -eq ${S_SUCCESS} ];
	do
		#
		# Read name of component
		#
		read comp

		if [ ${?} -eq 0 ]; then
			if [ ${_skip} -eq 1 ] &&
				[ "${comp}" == "${S_HISTORY_COMP}" ]; then
				_skip=0
			else
				ShowSkip ${comp}
			fi

			if [ ${_skip} -eq 0 ]; then
				ModHistory ${S_ATTR_HISTORY_COMP} ${comp}

				DoConfigureComp ${comp}
				_ret=${?}

				retval=${_ret}
			fi
		else
			_eof=1
			retval=$S_FAILURE
		fi

	done < ${_cfg}

	[ ${_eof} -eq 1 ] && retval=${_ret}

	return $retval
}

#
# Compile a component
#
function DoCompileComp
{
	local retval=$S_SUCCESS
	local _cmp=$1

	GetConfig $_cmp

	ShowComponent

	pushd $S_COMP_PATH > /dev/null

	DoStepsCompile
	retval=${?}

	popd > /dev/null

	#
	# Unset all component specific flags
	#
	[ -n "${S_COMP_MAKE_ABS}" ] && unset S_COMP_MAKE_ABS

	[ -n "${S_COMP_MAKE_ENV}" ] && unset S_COMP_MAKE_ENV
	[ -n "${S_COMP_MAKE_CMD}" ] && unset S_COMP_MAKE_CMD
	[ -n "${S_COMP_MAKE_ARG}" ] && unset S_COMP_MAKE_ARG

	return $retval
}

#
# Compiles a list of components
# Calls DoCompileComp() for each component in the list
#
function DoCompileList
{
	local retval=$S_SUCCESS
	local _list=$1
	local _skip=$2

	local _cfg=./configs/list/${_list}.cfg
	local _eof=0
	local _ret

	if [ ${_skip} -eq 0 ]; then
		ModHistory ${S_ATTR_HISTORY_LIST}   ${_list}
		ModHistory ${S_ATTR_HISTORY_ACTION} ${S_ACT}
	fi

	while [ ${retval} -eq ${S_SUCCESS} ];
	do
		#
		# Read name of component
		#
		read comp

		if [ ${?} -eq 0 ]; then
			if [ ${_skip} -eq 1 ] &&
				[ "${comp}" == "${S_HISTORY_COMP}" ]; then
				_skip=0
			else
				ShowSkip ${comp}
			fi

			if [ ${_skip} -eq 0 ]; then
				ModHistory ${S_ATTR_HISTORY_COMP} ${comp}

				DoCompileComp ${comp}
				_ret=${?}

				retval=${_ret}
			fi
		else
			_eof=1
			retval=$S_FAILURE
		fi

	done < ${_cfg}

	[ ${_eof} -eq 1 ] && retval=${_ret}

	return $retval
}

#
# Clean a component
#
function DoCleanComp
{
	local retval=$S_SUCCESS
	local _cmp=$1

	GetConfig $_cmp

	ShowComponent

	pushd $S_COMP_PATH > /dev/null

	DoStepsClean
	retval=${?}

	popd > /dev/null

	return $retval
}

#
# Clean a list of components
# Calls DoCleanComp() for each component in the list
#
function DoCleanList
{
	local retval=$S_SUCCESS
	local _list=$1
	local _skip=$2

	local _cfg=./configs/list/${_list}.cfg
	local _eof=0
	local _ret

	local _fail=()

	if [ ${_skip} -eq 0 ]; then
		ModHistory ${S_ATTR_HISTORY_LIST}   ${_list}
		ModHistory ${S_ATTR_HISTORY_ACTION} ${S_ACT}
	fi

	while [ ${_eof} -eq 0 ];
	do
		#
		# Read name of component
		#
		read comp

		if [ ${?} -eq 0 ]; then
			if [ ${_skip} -eq 1 ] &&
				[ "${comp}" == "${S_HISTORY_COMP}" ]; then
				_skip=0
			else
				ShowSkip ${comp}
			fi

			if [ ${_skip} -eq 0 ]; then
				ModHistory ${S_ATTR_HISTORY_COMP} ${comp}

				DoCleanComp ${comp}

				[ ${?} -eq ${S_FAILURE} ] && _fail+=(${comp})
			fi
		else
			_eof=1
		fi

	done < ${_cfg}

	if [ ${#_fail[@]} -ne 0 ]; then

		ShowSpacer $S_LEVEL0

		ShowSpacer $S_LEVEL1
		ShowInfo $S_LEVEL1 "Ignored failures while cleaning:"
		ShowSpacer $S_LEVEL1

		for _i in ${_fail[@]}
		do
			ShowInfo $S_LEVEL4 "${_i}"
		done

		ShowSpacer $S_LEVEL0
	fi

	return $retval
}

#
# Install a component - necessary when creating filesystem from pre-built
# components OR updating the (version of) component itself.
#
function DoInstallComp
{
	local retval=$S_SUCCESS
	local _cmp=$1

	GetConfig $_cmp

	ShowComponent

	pushd $S_COMP_PATH > /dev/null

	DoStepsInstall
	retval=${?}

	popd > /dev/null

	#
	# Unset all component specific flags
	#
	[ -n "${S_COMP_INST_ABS}" ] && unset S_COMP_INST_ABS

	[ -n "${S_COMP_INST_ENV}" ] && unset S_COMP_INST_ENV
	[ -n "${S_COMP_INST_CMD}" ] && unset S_COMP_INST_CMD
	[ -n "${S_COMP_INST_ARG}" ] && unset S_COMP_INST_ARG

	return $retval
}

#
# Install a list of components
# Calls DoInstallComp() for each component in the list
#
function DoInstallList
{
	local retval=$S_SUCCESS
	local _list=$1
	local _skip=$2

	local _cfg=./configs/list/${_list}.cfg
	local _eof=0
	local _ret

	if [ ${_skip} -eq 0 ]; then
		ModHistory ${S_ATTR_HISTORY_LIST}   ${_list}
		ModHistory ${S_ATTR_HISTORY_ACTION} ${S_ACT}
	fi

	while [ ${retval} -eq ${S_SUCCESS} ];
	do
		#
		# Read name of component
		#
		read comp

		if [ ${?} -eq 0 ]; then
			if [ ${_skip} -eq 1 ] &&
				[ "${comp}" == "${S_HISTORY_COMP}" ]; then
				_skip=0
			else
				ShowSkip ${comp}
			fi

			if [ ${_skip} -eq 0 ]; then
				ModHistory ${S_ATTR_HISTORY_COMP} ${comp}

				DoInstallComp ${comp}
				_ret=${?}

				retval=${_ret}
			fi
		else
			_eof=1
			retval=$S_FAILURE
		fi

	done < ${_cfg}

	[ ${_eof} -eq 1 ] && retval=${_ret}

	return $retval
}

#
# Build (configure, compile and install) a component
#
function DoBuildComp
{
	local retval=$S_SUCCESS
	local _cmp=$1

	GetConfig $_cmp

	ShowComponent

	pushd $S_COMP_PATH > /dev/null

	DoStepsConfigure
	retval=${?}

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		DoStepsCompile
		retval=${?}
	fi

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		DoStepsInstall
		retval=${?}
	fi

	popd > /dev/null

	#
	# Unset all component specific flags
	#
	[ -n "${S_COMP_CONF_ABS}" ] && unset S_COMP_CONF_ABS
	[ -n "${S_COMP_MAKE_ABS}" ] && unset S_COMP_MAKE_ABS
	[ -n "${S_COMP_INST_ABS}" ] && unset S_COMP_INST_ABS

	[ -n "${S_COMP_CONF_ENV}" ] && unset S_COMP_CONF_ENV
	[ -n "${S_COMP_CONF_CMD}" ] && unset S_COMP_CONF_CMD
	[ -n "${S_COMP_CONF_ARG}" ] && unset S_COMP_CONF_ARG

	[ -n "${S_COMP_MAKE_ENV}" ] && unset S_COMP_MAKE_ENV
	[ -n "${S_COMP_MAKE_CMD}" ] && unset S_COMP_MAKE_CMD
	[ -n "${S_COMP_MAKE_ARG}" ] && unset S_COMP_MAKE_ARG

	[ -n "${S_COMP_INST_ENV}" ] && unset S_COMP_INST_ENV
	[ -n "${S_COMP_INST_CMD}" ] && unset S_COMP_INST_CMD
	[ -n "${S_COMP_INST_ARG}" ] && unset S_COMP_INST_ARG

	return $retval
}

#
# Build (configure, compile and install) a list of components
# Calls DoBuildComp() for each component in the list
#
function DoBuildList
{
	local retval=$S_SUCCESS
	local _list=$1
	local _skip=$2

	local _cfg=./configs/list/${_list}.cfg
	local _eof=0
	local _ret

	if [ ${_skip} -eq 0 ]; then
		ModHistory ${S_ATTR_HISTORY_LIST}   ${_list}
		ModHistory ${S_ATTR_HISTORY_ACTION} ${S_ACT}
	fi

	while [ ${retval} -eq ${S_SUCCESS} ];
	do
		#
		# Read name of component
		#
		read comp

		if [ ${?} -eq 0 ]; then
			if [ ${_skip} -eq 1 ] &&
				[ "${comp}" == "${S_HISTORY_COMP}" ]; then
				_skip=0
			else
				ShowSkip ${comp}
			fi

			if [ ${_skip} -eq 0 ]; then
				ModHistory ${S_ATTR_HISTORY_COMP} ${comp}

				DoBuildComp ${comp}
				_ret=${?}

				retval=${_ret}
			fi
		else
			_eof=1
			retval=$S_FAILURE
		fi

	done < ${_cfg}

	[ ${_eof} -eq 1 ] && retval=${_ret}

	return $retval
}

#
# Get version of a specific component.
#
function GetVersionComp
{
	local retval=$S_SUCCESS
	local _cmp=$1
	local _lst=$2

	GetConfig $_cmp

	if [ "${_lst}" == "list" ]; then
		printf "      : %-16s : v%-10s\n" ${S_COMP_NAME} ${S_COMP_VERS}
	else
		printf "      : %s - v%s\n" ${S_COMP_NAME} ${S_COMP_VERS}
	fi

	return $retval
}

#
# Get version of all components in the list.
# Calls GetVersionComp() for each component in the list
#
function GetVersionList
{
	local _list=$1

	local _cfg=./configs/list/${_list}.cfg
	local _eof=0
	local _cmp

	while [ ${_eof} -eq 0 ];
	do
		#
		# Read name of component
		#
		read _cmp

		if [ ${?} -eq 0 ]; then
			GetVersionComp ${_cmp} "list"
		else
			_eof=1
		fi

	done < ${_cfg}

	return $S_SUCCESS
}

#
# Select the busybox configuration to be used for building the filesystem.
#
function SelectBbConfig
{
	local retval=$S_SUCCESS
	local _sel
	local _cfg


	ShowStep $S_LEVEL1 "Select busybox configuration"

	#
	# Get the attribute corresponding to busybox configuration
	#
	GetAttribute bbconfig

	if [ ${?} -eq $S_SUCCESS ]; then
		_sel=$S_RETSTR

		ShowInfo $S_LEVEL1 "Using selected busybox config - ${_sel}"
	else
		_sel='default'

		ShowInfo $S_LEVEL1 "Using default busybox config"
	fi

	#
	# Check if the config file exists
	#
	_cfg=./configs/busybox/${_sel}.config

	if [ -f ${_cfg} ]; then

		[ -f ${S_CFG_BUSYBOX} ] && \rm -f ${S_CFG_BUSYBOX}

		cp ${_cfg} ${S_CFG_BUSYBOX}

		if [ $? -ne 0 ]; then

			ShowError $S_LEVEL2 "Unable to copy busybox config :("

			retval=$S_FAILURE
		fi

	else

		ShowSpacer $S_LEVEL2
		ShowError  $S_LEVEL2 "busybox config not found - ${_cfg}"
		ShowSpacer $S_LEVEL2

		retval=$S_FAILURE
	fi

	return $retval
}

function UpdateCatalog
{
	local retval=$S_SUCCESS

	local _pkg=$1
	local _ver=$2

	local _cat=${S_INSTALL}${S_CATALOG}
	local _dir=$(dirname ${_cat})

	#
	# Ensure that catalog exists
	#
	if [ ! -d ${_dir} ]; then
		mkdir -p ${_dir}
		[ $? -ne 0 ] && retval=$S_FAILURE
	fi

	if [ ${retval} -eq ${S_SUCCESS} ] &&
	   [ ! -f ${_cat} ]; then
		touch ${_cat}
		[ $? -ne 0 ] && retval=$S_FAILURE
	fi

	if [ ${retval} -eq ${S_SUCCESS} ]; then
		#
		# Delete any existing reference to the package.
		# Append package information to the end.
		#
		sed -i "/^${_pkg}/d" ${_cat}

		echo -e "${_pkg}\t\t\t( ${_ver} )" >> ${_cat}

		if [ $? -ne 0 ]; then
			ShowError $S_LEVEL2 "Unable to update catalog :("
			retval=$S_FAILURE
		fi
	else
		ShowError $S_LEVEL2 "Unable to create catalog :("
	fi

	return $retval
}

#
# Many components don't "provide" right information for cross-compiling
# in the generated libtool control files (*.la).
#
# This function does necessary corrections.
#
function PostProcess
{
	local retval=$S_SUCCESS
	local _since=$@

	pushd ${S_INSTALL} > /dev/null

	#
	# Find all .la files modified since the start of install step
	#
	# The minute granularity may bring in additional files, but
	# should ensure that nothing gets missed.
	#
	# The regex is quite robust to prevent any 'pollution'
	# even if the file is processed multiple times.
	#
	local _lst=$(find . -type f -newermt "${_since}" -name "*.la" -print)

	if [ "S${_lst}" != "S" ]; then

		for _f in ${_lst}
		do
			ShowInfo $S_LEVEL2 "${_f}"

			#
			# Ensure all files are located in $S_INSTALL
			#
			perl -pi -e "if (/^dependency_libs/) { s#\s(/usr.*?\.la)# ${S_INSTALL}\1#g; }" ${_f}
		done
	fi

	popd > /dev/null

	return $retval
}
