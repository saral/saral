#!/bin/bash
#
# saral/include/derived.sh
#
# Defines commonly used variables derived from existing variables.
#
#
# The MIT License
#
# Copyright (c) 2009-2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ------------------------------------------------------------------------------
# From S_BASE
# ------------------------------------------------------------------------------
#
# Default location of source packages
#
# RECOMMENDATION: This path should be a symbolic link to location outside this
#                 repository
#
S_SRC_PACKAGES=${S_BASE}/src-packages

#
# Default location of download source packages (compressed)
# If a package is available here, it won't be downloaded again
#
# RECOMMENDATION: This path should be a symbolic link to location outside this
#                 repository
#
S_PKG_CACHE=${S_BASE}/pkg-cache

#
# Base of static content included in the root filesystem.
#
S_SRC_ROOTFS=${S_BASE}/src-rfs

#
# Target install path
# Used as argument to 'make install' step
#
S_INSTALL=${S_BASE}/_install

#
# Target filesystem path
# The target filesystem will be assembled at this location
#
# While assembling the filesystem, ownerships and permissions are changed
# (via sudo/fakeroot). It is, therefore, prudent to keep this path different
# from S_INSTALL defined above.
#
S_FILESYS=${S_BASE}/_filesys

#
# Busybox configuration
#
# Contains the busybox configuration to be used for
# creating the filesystem.
#
S_CFG_BUSYBOX=${S_BASE}/.saral-bb-config


# ------------------------------------------------------------------------------
# From S_KERNEL
# ------------------------------------------------------------------------------
#
# Userspace headers exported via:
#   make headers_install INSTALL_HDR_PATH=./_install
#
S_KERN_INC=${S_KERNEL}/_install/include


# ------------------------------------------------------------------------------
# From S_INSTALL
# ------------------------------------------------------------------------------
#
# Search path for header files
#
S_INST_USRINC=${S_INSTALL}/usr/include
S_INST_USRLOCALINC=${S_INSTALL}/usr/local/include

#
# Search path for libraries
#
S_INST_LIB=${S_INSTALL}/lib
S_INST_USRLIB=${S_INSTALL}/usr/lib
S_INST_USRLOCALLIB=${S_INSTALL}/usr/local/lib


# ------------------------------------------------------------------------------
# From S_FILESYS
# ------------------------------------------------------------------------------
#
# Using additional level allows generated filesystem images to be contained
# under same base location - S_FILESYS.
#
S_RFS_ASSEMBLE=${S_FILESYS}/rootfs

#
# All auxiliary packages (e.g devkit, locales, etc.) are located here
#
S_AUX_BASE=${S_FILESYS}/aux

#
# Source for DevKit package
# Contains headers, objects and libraries
#
S_AUX_DEVKIT=${S_AUX_BASE}/devkit

#
# Source for Help package
# Contains man pages and info documents
#
S_AUX_HELP=${S_AUX_BASE}/help

#
# Source for terminfo database
# Contains definitions for additional terminals
#
S_AUX_TERMDB=${S_AUX_BASE}/termdb

#
# Source for internationalization
# Contains information necessary for internationalization, localization and timezones
#
S_AUX_INTL=${S_AUX_BASE}/intl
