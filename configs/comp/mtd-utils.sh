#!/bin/bash
#
# saral/configs/comp/mtdutils.sh
#
# Configuration for package - mtdutils.
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=mtdutils

#
# Version of the package
#
S_COMP_VERS=1.5.1

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://git.infradead.org/mtd-utils.git/snapshot/9f107132a6a073cce37434ca9cda6917dd8d866b.tar.gz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/mtd-utils-9f10713


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# Steps to configure the component
#
S_COMP_CONF_ENV[0]=""

S_COMP_CONF_CMD[0]=""
S_COMP_CONF_ARG[0]=""


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_ENV[0]="CROSS=${S_X_PREFIX} WITHOUT_XATTR=1 WITHOUT_LARGEFILE=1"

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS=""


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_ENV[0]="CROSS=${S_X_PREFIX} WITHOUT_XATTR=1 WITHOUT_LARGEFILE=1"

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
