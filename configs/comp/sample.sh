#!/bin/bash
#
# saral/configs/comp/sample.sh
#
# Sample configuration to be used as template for actual components.
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=sample

#
# Version of the package
#
S_COMP_VERS=1.0

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://some.where.net/sample.tgz

#
# Path to the package sources in local machine
#
S_COMP_PATH=~/repos/sample


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# This flag indicates that component expects fixed command-line arguments.
# SARAL will not add generic flags/options.
#
# The flag is commented by default
#S_COMP_CONF_ABS=1

S_COMP_CONF_ENV[0]="SOME_FLAG=1"

S_COMP_CONF_CMD[0]="autogen.sh"
S_COMP_CONF_ARG[0]=""

S_COMP_CONF_ENV[1]="OTHER_FLAG=1"

S_COMP_CONF_CMD[1]="configure"
S_COMP_CONF_ARG[1]="--enable-some-thing"
S_COMP_CONF_ARG[1]+=" --disable-some-thing"

# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_ENV[0]="SOME_FLAG=1"

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]="SOME_VAR=1"
S_COMP_MAKE_ARG[0]+=" OTHER_VAR=2"

#
# Flags for compiler
#
S_COMP_CFLAGS='-I$WORKDIR/repos/sample/include'

#
# Flags for linker
#
S_COMP_LDFLAGS='-L$WORKDIR/repos/sample/lib'

#
# Steps to install the component
#
S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="SOME_VAR=1"
S_COMP_INST_ARG[0]+=" OTHER_VAR=2"


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_ENV[0]="SOME_FLAG=1"

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
