#!/bin/bash
#
# saral/configs/comp/SDL.sh
#
# Configuration for package - SDL.
#
#
# The MIT License
#
# Copyright (c) 2009-2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=SDL

#
# Version of the package
#
S_COMP_VERS=1.2.15

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://www.libsdl.org/release/SDL-${S_COMP_VERS}.tar.gz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/SDL-${S_COMP_VERS}


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# Steps to configure the component
#
S_COMP_CONF_CMD[0]="autogen.sh"
S_COMP_CONF_ARG[0]=""

S_COMP_CONF_CMD[1]="configure"
S_COMP_CONF_ARG[1]="--enable-joystick=no"
S_COMP_CONF_ARG[1]+=" --enable-cdrom=no"
S_COMP_CONF_ARG[1]+=" --enable-oss=no"
S_COMP_CONF_ARG[1]+=" --enable-pulseaudio=no"
S_COMP_CONF_ARG[1]+=" --enable-arts=no"
S_COMP_CONF_ARG[1]+=" --enable-nas=no"
S_COMP_CONF_ARG[1]+=" --enable-diskaudio=no"
S_COMP_CONF_ARG[1]+=" --enable-dummyaudio=no"
S_COMP_CONF_ARG[1]+=" --enable-mintaudio=no"
S_COMP_CONF_ARG[1]+=" --enable-nasm=no"
S_COMP_CONF_ARG[1]+=" --enable-altivec=no"
S_COMP_CONF_ARG[1]+=" --enable-video-x11=no"
S_COMP_CONF_ARG[1]+=" --enable-dga=no"
S_COMP_CONF_ARG[1]+=" --enable-video-photon=no"
S_COMP_CONF_ARG[1]+=" --enable-video-carbon=no"
S_COMP_CONF_ARG[1]+=" --enable-video-cocoa=no"
S_COMP_CONF_ARG[1]+=" --enable-video-fbcon=no"
S_COMP_CONF_ARG[1]+=" --enable-video-directfb=no"
S_COMP_CONF_ARG[1]+=" --enable-video-ps2gs=no"
S_COMP_CONF_ARG[1]+=" --enable-video-ps3=no"
S_COMP_CONF_ARG[1]+=" --enable-video-ggi=no"
S_COMP_CONF_ARG[1]+=" --enable-video-svga=no"
S_COMP_CONF_ARG[1]+=" --enable-video-vgl=no"
S_COMP_CONF_ARG[1]+=" --enable-video-wscons=no"
S_COMP_CONF_ARG[1]+=" --enable-video-aalib=no"
S_COMP_CONF_ARG[1]+=" --enable-video-caca=no"
S_COMP_CONF_ARG[1]+=" --enable-video-qtopia=no"
S_COMP_CONF_ARG[1]+=" --enable-video-picogui=no"
S_COMP_CONF_ARG[1]+=" --enable-video-dummy=no"
S_COMP_CONF_ARG[1]+=" --enable-video-opengl=no"
S_COMP_CONF_ARG[1]+=" --enable-input-events=no"
S_COMP_CONF_ARG[1]+=" --enable-input-tslib=no"
S_COMP_CONF_ARG[1]+=" --enable-stdio-redirect=no"
S_COMP_CONF_ARG[1]+=" --enable-directx=no"
S_COMP_CONF_ARG[1]+=" --enable-sdl-dlopen=no"
S_COMP_CONF_ARG[1]+=" --enable-atari-ldg=no"
S_COMP_CONF_ARG[1]+=" --program-prefix=\"\""


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS=""


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
