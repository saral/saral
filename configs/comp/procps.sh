#!/bin/bash
#
# saral/configs/comp/procps.sh
#
# Configuration for package - procps.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=procps

#
# Version of the package
#
S_COMP_VERS=3.2.8

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://procps.sourceforge.net/procps-3.2.8.tar.gz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/procps-3.2.8


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# Steps to configure the component
#
S_COMP_CONF_CMD[0]="sed"
S_COMP_CONF_ARG[0]="-i 's#I/usr/include/ncurses#I\$(S_INSTALL)/usr/include#' Makefile"

S_COMP_CONF_CMD[1]="sed"
S_COMP_CONF_ARG[1]="-i -e '/^ALL_CPPFLAGS[ ]/ s/$/ \$(CXXFLAGS)/' Makefile"

S_COMP_CONF_CMD[2]="sed"
S_COMP_CONF_ARG[2]="-i -e 's/^LDFLAGS[ ]*:=.*/LDFLAGS += -L./' Makefile"

S_COMP_CONF_CMD[3]="sed"
S_COMP_CONF_ARG[3]="-i -e 's/^ldconfig[ ]*:=.*/ldconfig :=/' Makefile"

S_COMP_CONF_CMD[4]="sed"
S_COMP_CONF_ARG[4]="-i -e 's#^-include[ ]*\*/module.mk#-include proc/module.mk ps/module.mk#' Makefile"

S_COMP_CONF_CMD[5]="sed"
S_COMP_CONF_ARG[5]="-i -e 's/^lib64[ ]*:=.*/lib64 := lib/' Makefile"

S_COMP_CONF_CMD[6]="sed"
S_COMP_CONF_ARG[6]="-i -e 's/^m64[ ]*:=.*/m64 :=/' Makefile"

S_COMP_CONF_CMD[7]="sed"
S_COMP_CONF_ARG[7]="-i -e 's/install -D --owner 0 --group 0/install -D/' Makefile"


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_ENV[0]="S_INSTALL=${S_INSTALL} S_INST_USRLIB=${S_INST_USRLIB} "
S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS="-ltinfo"


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
