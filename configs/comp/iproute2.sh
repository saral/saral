#!/bin/bash
#
# saral/configs/comp/iproute2.sh
#
# Configuration for package - iproute2.
#
#
# The MIT License
#
# Copyright (c) 2012-2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=iproute2

#
# Version of the package
#
S_COMP_VERS=3.12.0

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://www.kernel.org/pub/linux/utils/net/iproute2/iproute2-${S_COMP_VERS}.tar.xz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/iproute2-${S_COMP_VERS}


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

S_COMP_CONF_CMD[0]="sed"
S_COMP_CONF_ARG[0]="-i 's/^CC\s=\sgcc/CC = \$(CROSS_COMPILE)gcc/' Makefile"

S_COMP_CONF_CMD[1]="sed"
S_COMP_CONF_ARG[1]="-i 's/^gcc/\$CC/' configure"

S_COMP_CONF_CMD[2]="sed"
S_COMP_CONF_ARG[2]="-i 's!for dir in /lib /usr/lib /usr/local/lib!for dir in \$S_INSTALL/lib \$S_INSTALL/usr/lib \$S_INSTALL/usr/local/lib!' configure"

S_COMP_CONF_CMD[3]="sed"
S_COMP_CONF_ARG[3]="-i '/^TARGETS/s@arpd@@g' misc/Makefile"


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]="S_INSTALL=${S_INSTALL}"

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS=""


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
