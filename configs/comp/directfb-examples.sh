#!/bin/bash
#
# saral/configs/comp/directfb-examples.sh
#
# Configuration for package - directfb-examples.
#
#
# The MIT License
#
# Copyright (c) 2009-2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=directfb-examples

#
# Version of the package
#
S_COMP_VERS=1.2.0

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://directfb.org/downloads/Extras/DirectFB-examples-1.2.0.tar.gz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/DirectFB-examples-1.2.0


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# Steps to configure the component
#
S_COMP_CONF_CMD[0]="configure"
S_COMP_CONF_ARG[0]=""


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS="-lz"


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
