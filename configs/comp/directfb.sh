#!/bin/bash
#
# saral/configs/comp/directfb.sh
#
# Configuration for package - directfb.
#
#
# The MIT License
#
# Copyright (c) 2009-2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=directfb

#
# Version of the package
#
S_COMP_VERS=1.4.15

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://directfb.org/downloads/Core/DirectFB-1.4/DirectFB-1.4.15.tar.gz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/DirectFB-1.4.15


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# Steps to configure the component
#
S_COMP_CONF_CMD[0]="autogen.sh"
S_COMP_CONF_ARG[0]=""

S_COMP_CONF_CMD[1]="configure"
S_COMP_CONF_ARG[1]="--enable-shared"
S_COMP_CONF_ARG[1]+=" --with-gfxdrivers=none"
S_COMP_CONF_ARG[1]+=" --with-inputdrivers=linuxinput,tslib"
S_COMP_CONF_ARG[1]+=" --enable-devmem"
S_COMP_CONF_ARG[1]+=" --enable-fbdev"
S_COMP_CONF_ARG[1]+=" --enable-zlib"
S_COMP_CONF_ARG[1]+=" --enable-sysfs"
S_COMP_CONF_ARG[1]+=" --enable-jpeg"
S_COMP_CONF_ARG[1]+=" --enable-png"
S_COMP_CONF_ARG[1]+=" --enable-freetype"
S_COMP_CONF_ARG[1]+=" --disable-sdl"
S_COMP_CONF_ARG[1]+=" --disable-network"
S_COMP_CONF_ARG[1]+=" --disable-gif"
S_COMP_CONF_ARG[1]+=" --disable-osx"
S_COMP_CONF_ARG[1]+=" --disable-x11"
S_COMP_CONF_ARG[1]+=" --disable-mmx"
S_COMP_CONF_ARG[1]+=" --disable-sse"
S_COMP_CONF_ARG[1]+=" --disable-vnc"
S_COMP_CONF_ARG[1]+=" --disable-linotype"
S_COMP_CONF_ARG[1]+=" --disable-video4linux"
S_COMP_CONF_ARG[1]+=" --disable-video4linux2"
S_COMP_CONF_ARG[1]+=" --with-gnu-ld"
S_COMP_CONF_ARG[1]+=" --prefix=/usr"


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS=""


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
