#!/bin/bash
#
# saral/configs/comp/moc.sh
#
# Configuration for package - moc.
#
#
# The MIT License
#
# Copyright (c) 2011-2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=moc

#
# Version of the package
#
S_COMP_VERS=2.4.4

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=ftp://ftp.daper.net/pub/soft/moc/stable/moc-${S_COMP_VERS}.tar.bz2

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/moc-${S_COMP_VERS}


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# Steps to configure the component
#
S_COMP_CONF_ENV[0]="ac_cv_func_malloc_0_nonnull=yes"

S_COMP_CONF_CMD[0]="configure"
S_COMP_CONF_ARG[0]="--with-ncurses"
S_COMP_CONF_ARG[0]+=" --with-mp3"
S_COMP_CONF_ARG[0]+=" --with-flac"
S_COMP_CONF_ARG[0]+=" --with-speex"
S_COMP_CONF_ARG[0]+=" --with-vorbis"
S_COMP_CONF_ARG[0]+=" --with-curl"
S_COMP_CONF_ARG[0]+=" --prefix=/usr"


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS="-ltinfo"


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
