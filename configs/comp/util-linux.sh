#!/bin/bash
#
# saral/configs/comp/util-linux.sh
#
# Configuration for package - util-linux.
#
#
# The MIT License
#
# Copyright (c) 2013 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=util-linux

#
# Version of the package
#
S_COMP_VERS=2.24

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=https://www.kernel.org/pub/linux/utils/util-linux/v${S_COMP_VERS}/util-linux-${S_COMP_VERS}.tar.xz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/util-linux-${S_COMP_VERS}


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

S_COMP_CONF_CMD[0]="configure"
S_COMP_CONF_ARG[0]="--with-gnu-ld"
S_COMP_CONF_ARG[0]+=" --with-ncurses"
S_COMP_CONF_ARG[0]+=" --enable-arch"
S_COMP_CONF_ARG[0]+=" --disable-rpath"
S_COMP_CONF_ARG[0]+=" --disable-gtk-doc"
S_COMP_CONF_ARG[0]+=" --disable-elvtune"
S_COMP_CONF_ARG[0]+=" --disable-nls"
S_COMP_CONF_ARG[0]+=" --disable-deprecated-mount"
S_COMP_CONF_ARG[0]+=" --disable-cramfs"
S_COMP_CONF_ARG[0]+=" --disable-login"
S_COMP_CONF_ARG[0]+=" --disable-su"
S_COMP_CONF_ARG[0]+=" --disable-use-tty-group"
S_COMP_CONF_ARG[0]+=" --disable-makeinstall-chown"
S_COMP_CONF_ARG[0]+=" --disable-makeinstall-setuid"
S_COMP_CONF_ARG[0]+=" --without-selinux"
S_COMP_CONF_ARG[0]+=" --prefix=/usr"


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS="-ltinfo"


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
