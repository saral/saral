#!/bin/bash
#
# saral/configs/comp/nodejs.sh
#
# Configuration for package - nodejs.
#
#
# The MIT License
#
# Copyright (c) 2014 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=nodejs

#
# Version of the package
#
S_COMP_VERS=0.10.31

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://nodejs.org/dist/v${S_COMP_VERS}/node-v${S_COMP_VERS}.tar.gz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/node-v${S_COMP_VERS}


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# Steps to configure the component
#
S_COMP_CONF_ABS['CMD']=1

S_COMP_CONF_CMD[0]="configure"
S_COMP_CONF_ARG[0]="--dest-cpu=arm"
S_COMP_CONF_ARG[0]+=" --with-arm-float-abi=hard"
S_COMP_CONF_ARG[0]+=" --dest-os=linux"
S_COMP_CONF_ARG[0]+=" --without-snapshot"
S_COMP_CONF_ARG[0]+=" --shared-openssl"
S_COMP_CONF_ARG[0]+=" --shared-zlib"


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS=""


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
