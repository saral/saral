#!/bin/bash
#
# saral/configs/comp/bzip2.sh
#
# Configuration for package - bzip2.
#
#
# The MIT License
#
# Copyright (c) 2011 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=bzip2

#
# Version of the package
#
S_COMP_VERS=1.0.6

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/bzip2-1.0.6


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

S_COMP_CONF_CMD[0]="sed"
S_COMP_CONF_ARG[0]="-i 's/CC=gcc/CC=\$(CROSS_COMPILE)gcc/' Makefile"

S_COMP_CONF_CMD[1]="sed"
S_COMP_CONF_ARG[1]="-i 's/AR=ar/AR=\$(CROSS_COMPILE)ar/' Makefile"

S_COMP_CONF_CMD[2]="sed"
S_COMP_CONF_ARG[2]="-i 's/RANLIB=ranlib/RANLIB=\$(CROSS_COMPILE)ranlib/' Makefile"

S_COMP_CONF_CMD[3]="sed"
S_COMP_CONF_ARG[3]="-i 's/CFLAGS=/CFLAGS+=/' Makefile"

S_COMP_CONF_CMD[4]="sed"
S_COMP_CONF_ARG[4]="-i 's/PREFIX=/PREFIX?=/' Makefile"

S_COMP_CONF_CMD[5]="sed"
S_COMP_CONF_ARG[5]="-i \"57iifeq (\$\(CROSS_COMPILE\),)\" Makefile"

S_COMP_CONF_CMD[6]="sed"
S_COMP_CONF_ARG[6]="-i \"72iendif\" Makefile"

S_COMP_CONF_CMD[7]="sed"
S_COMP_CONF_ARG[7]="-i 's!\$(PREFIX)/include!\$(PREFIX)/usr/include!g' Makefile"

S_COMP_CONF_CMD[8]="sed"
S_COMP_CONF_ARG[8]="-i 's!\$(PREFIX)/lib!\$(PREFIX)/usr/lib!g' Makefile"

# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS=""


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
S_COMP_INST_ARG[0]+=" PREFIX=${S_INSTALL}"
