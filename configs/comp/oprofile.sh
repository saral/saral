#!/bin/bash
#
# saral/configs/comp/oprofile.sh
#
# Configuration for package - oprofile.
#
#
# The MIT License
#
# Copyright (c) 2009 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# SECTION 1: INFORMATION
# ==============================================================================

#
# Name of the package
#
S_COMP_NAME=oprofile

#
# Version of the package
#
S_COMP_VERS=0.9.6+

#
# Fully qualified path to fetch the source archive
#
S_COMP_WGET=http://oprofile.git.sourceforge.net/git/gitweb.cgi?p=oprofile/oprofile;a=snapshot;h=1aab0baa20f398ad016017c530b85608fb95be94;sf=tgz

#
# Path to the package sources in local machine
#
S_COMP_PATH=./src-packages/oprofile-1aab0ba


# ==============================================================================
# SECTION 2: STEPS TO CONFIGURE
# ==============================================================================

#
# This flag indicates that component expects fixed command-line arguments.
# SARAL will not add generic flags/options.
#
S_COMP_CONF_ABS=1

#
# Steps to configure the component
#
S_COMP_CONF_CMD[0]="autogen.sh"
S_COMP_CONF_ARG[0]=""

S_COMP_CONF_CMD[1]="configure"
S_COMP_CONF_ARG[1]=${S_X_VARS}
S_COMP_CONF_ARG[1]+=" --with-kernel-support"
S_COMP_CONF_ARG[1]+=" --with-gnu-ld"


# ==============================================================================
# SECTION 3: STEPS TO COMPILE
# ==============================================================================

S_COMP_MAKE_CMD[0]="make"
S_COMP_MAKE_ARG[0]=""

#
# Flags for compiler
#
S_COMP_CFLAGS=""

#
# Flags for linker
#
S_COMP_LDFLAGS=""


# ==============================================================================
# SECTION 4: STEPS TO INSTALL
# ==============================================================================

S_COMP_INST_CMD[0]="make"
S_COMP_INST_ARG[0]="install"
